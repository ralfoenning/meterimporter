package com.lightsource.monitoring.meterimport

import java.io.File
import java.nio.file.Path
import scala.collection.JavaConversions.asScalaBuffer
import scala.xml.Node
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.mongo.FongoStorage
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ConsumptionCreationTest2 extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "simpleCase subtracting one 19/05 day from two days 19/05-20/05" should {

    "work" in {

      val aslf = fileResource("asl-20150519.csv")
      new MeterDataHandling[String, Path]().process(aslf.toPath(), new MeterDataCsvFunctions(FongoStorage))
      val imsf = fileResource("imserv2days1meter_241.xml")
      new MeterDataHandling[Node, Path].process(imsf.toPath(), new MeterDataXml(FongoStorage))

      val res = FongoStorage.meterdb.getCollection("readings").find().toArray()
      import scala.collection.JavaConversions._
      println(res.toList.groupBy(_.get("type")).size)

      println(res.toList.filter(_.get("type").asInstanceOf[String] == "generation").size)
      println(res.toList.filterNot(_.get("type").asInstanceOf[String] == "generation").size)

      val timeserieszip = res.toList.filter(_.get("type").asInstanceOf[String] == "generation").zip(res.toList.filter(_.get("type").asInstanceOf[String] == "export"))
      
      println(timeserieszip.size)
      println(timeserieszip)
      
      
      val timeseriesdiff = timeserieszip.map {

        tup =>
          {
            val gendbo = tup._1
            val expdbo = tup._2

            val genVal = gendbo.get("value").asInstanceOf[Double]
            val expVal = expdbo.get("value").asInstanceOf[Double]

            genVal - expVal

          }
      }
      
      println(timeseriesdiff)


      ok
    }
  }
}