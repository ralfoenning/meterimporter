package com.lightsource.monitoring.alerting

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import com.lightsource.monitoring.common.MonitoringStorage
import com.lightsource.monitoring.common.MongoStorage
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.commons.MongoDBObject
import org.joda.time.LocalDate
import java.util.Date

class StaleDetector(rep: MonitoringStorage, args: Array[String]) {

  println("hello from StaleDetector")

  val mostRecentEntries = rep.allknownMeterids().flatMap { each => rep.mostrecentEntryFor(each) }

  val p = ISODateTimeFormat.dateOptionalTimeParser()
  val timestampOf = (e: DBObject) => new DateTime(e.get("timestamp").asInstanceOf[Date])
  val actions =
    for (e <- mostRecentEntries if (timestampOf(e).isBefore(DateTime.now().minusHours(33))) ) yield {
        Alerting(rep).produceAlarmActions(None)(
          e.get("type").asInstanceOf[String],
          e.get("meterId").asInstanceOf[String],
          timestampOf(e).toLocalDate(), 
          List(), 
          List.fill[Double](48)(0.0))
    }
  
  actions.flatten[AlarmActioner] map { a => a.doit() } 

}

object StaleDetector extends App {

  new StaleDetector(MongoStorage, args)

}
  
