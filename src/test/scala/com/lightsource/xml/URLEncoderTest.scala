package com.lightsource.xml

import java.io.File
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import scala.runtime.ScalaRunTime
import org.joda.time.DateTime
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import java.net.URLEncoder

@RunWith(classOf[JUnitRunner])
class URLEncoderTest extends Specification {

  "url encoding" should {

    "work" in {

      println(URLEncoder.encode("""{meterId:"EML123"}""", "UTF-8"))

      ok
    }
  }

}