package com.lightsource.xml

import java.io.File
import scala.xml.Node
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.specs2.mutable.Specification
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ImServXMLTest2 extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())
  def streamResource(resourcePath: String) = this.getClass().getResourceAsStream("/" + resourcePath)

  "reading imserv xml file" should {

    val oneday =
      <Report Name="RPT_CON2_BasicReportBuilder" Textbox22="***** No Data Available for the selected report parameters *****" Textbox76="Report displays data in Time Period End" xmlns="RPT_CON2_BasicReportBuilder" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="RPT_CON2_BasicReportBuilder http://edvsql02lsc:8080/ReportServer?%2FEDV_Reports%2FBasicReportBuilder%2FRPT_CON2_BasicReportBuilder&amp;rs%3AFormat=XML&amp;rs%3ASnapshot%3Aisnull=True&amp;rc%3ASchema=True">
        <Tablix5>
          <Textbox33 Company_Name="Lightsource Midscale Limited" Textbox33="Company Name"/>
          <Textbox35 Measure_Type="Electricity" Textbox35="Measure Type"/>
          <Textbox263 MeasureLevelType_Name="Multiple" Textbox263="Measure Level Type"/>
          <Textbox37 MeasurementQuantity_Name="Consumption" Textbox37="Measure Quantity Converted To"/>
          <Textbox39 Measurementunit_Name="kWh" Textbox39="Measurement Unit Converted To"/>
        </Tablix5>
        <Tablix8>
          <Textbox41 Textbox41="Report At" Textbox42="Measure"/>
          <Textbox2 Textbox2="Aggregate Level" Textbox20="30 (minutes)"/>
          <Textbox249 Textbox249="Start Date" Textbox250="22/04/2015"/>
          <Textbox251 Textbox251="End Date" Textbox252="23/04/2015"/>
          <Textbox235 Textbox235="Transaction Timestamp" Textbox236="29/04/2015 19:04:00"/>
        </Tablix8>
        <Tablix6>
          <Textbox43 Textbox43="Portfolio Item 1" Textbox48="214669932 kWh Import"/>
          <Textbox44 Textbox44="Portfolio Item 2" Textbox49="Holme Mills POC 1 Import"/>
          <Textbox45 Textbox45="Portfolio Item 2"/>
          <Textbox46 Textbox46="Portfolio Item 4"/>
          <Textbox47 Textbox47="Portfolio Item 5"/>
        </Tablix6>
        <Tablix7>
          <Textbox53 Textbox53="Portfolio Item 6"/>
          <Textbox54 Textbox54="Portfolio Item 7"/>
          <Textbox55 Textbox55="Portfolio Item 8"/>
          <Textbox56 Textbox56="Portfolio Item 9"/>
          <Textbox57 Textbox57="Portfolio Item 10"/>
        </Tablix7>
        <Chart1>
          <Chart1_SeriesGroup_Collection>
            <Chart1_SeriesGroup Label="kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 01:00">
                  <Value Y="1.9"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 01:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 02:00">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 02:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 03:00">
                  <Value Y="2"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 03:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 04:00">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 04:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 05:00">
                  <Value Y="3.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 05:30">
                  <Value Y="8.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 06:00">
                  <Value Y="9.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 06:30">
                  <Value Y="9.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 07:00">
                  <Value Y="4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 07:30">
                  <Value Y="2.1"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 08:00">
                  <Value Y="0.3"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 08:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 09:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 09:30">
                  <Value Y="0.1"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 10:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 10:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 11:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 11:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 12:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 12:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 13:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 13:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 14:00">
                  <Value Y="0.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 14:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 15:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 15:30">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 16:00">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 16:30">
                  <Value Y="0.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 17:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 17:30">
                  <Value Y="2.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 18:00">
                  <Value Y="6.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 18:30">
                  <Value Y="7.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 19:00">
                  <Value Y="7.3"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 19:30">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 20:00">
                  <Value Y="1.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 20:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 21:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 21:30">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 22:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 22:30">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:30">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 00:00">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
          </Chart1_SeriesGroup_Collection>
        </Chart1>
      </Report>

    "work" in {

      println("===ONE DAY===")

      println("all times: " + (oneday \\ "Chart1_CategoryGroup").collect { case node: Node => node.attributes.get("Label") })
      println("all values:" + (oneday \\ "Value").collect { case node: Node => node.attributes.get("Y") })

      ok
    }
  }
}