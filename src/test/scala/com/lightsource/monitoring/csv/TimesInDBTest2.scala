package com.lightsource.monitoring.meterimport

import java.util.Date

import com.lightsource.monitoring.mongo.FongoStorage
import com.lightsource.monitoring.{MeterDataHandling, TimeSeriesStringToStringFunctions}
import com.mongodb.BasicDBObject
import org.joda.time.{DateTime, DateTimeZone}
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TimesInDBTest2 extends Specification {

  "times of showings" should {

    "work" in {

      FongoStorage.meterdb.getCollection("readings").drop()

      val srcLine = """A508100034,14172974,01:03:12 Mon 18/05/15,1086773.56,725.6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.26668,0,1.122,0,3.0396,0,6.7465,0,8.294,0,11.2435,0,12.3315,0,16.794,0,14.8515,0,21.4255,0,21.692,0,22.5385,0,27.124,0,0,0,26.053,0,0,0,18.0535,0,18.218,0,18.17,0,11.8545,0,8.0365,0,8.118,0,8.272,0,3.5705,0,2.1408,0,0.6293,0,0.06808,0.02872,0,0.07612,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""

      import com.lightsource.monitoring.configs.meterreadings.{aslgeneration => aslg}
      aslg.readingTimes(srcLine)(0).toString() must_== "2015-05-18T00:30:00.000Z"
      aslg.readingTimes(srcLine).last.toString() must_== "2015-05-19T00:00:00.000Z"

      new MeterDataHandling[String, String]().process(srcLine, new TimeSeriesStringToStringFunctions(FongoStorage))

      println(FongoStorage.meterdb.getCollection("readings").find(new BasicDBObject("type", "generation-showing") ).next())

      new DateTime(
        FongoStorage.meterdb.getCollection("readings").find(new BasicDBObject("type", "generation-showing") ).next().get("timestamp").asInstanceOf[Date]).
        toDateTime(DateTimeZone.UTC).
        toString() must_== "2015-05-18T00:30:00.000Z"

      ok

    }
  }

}