package com.lightsource.monitoring.alerting

import org.joda.time.DateTime
import com.lightsource.monitoring.alerting.AlarmAction.AlarmAction
import com.lightsource.monitoring.alerting.AlarmAction.CloseIfAny
import com.lightsource.monitoring.alerting.AlarmAction.New
import com.lightsource.monitoring.common.MongoStorage
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime

class ActionerWithTime(action: AlarmAction)(meterid: String, csvday: LocalDate, iso8601Times: List[String], daydvalues: List[Double]) extends AlarmActioner {

  def _doit() = action match {
    case CloseIfAny => MongoStorage.closeAlarm(meterid, csvday.toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC))
    case New => {
      //        <<<<>>>>installationscoll.find(ref, keys)
      MongoStorage.createAlarm(meterid, csvday.toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC))
    }
    case _ => {}
  }
  override def toString = action.toString()
}