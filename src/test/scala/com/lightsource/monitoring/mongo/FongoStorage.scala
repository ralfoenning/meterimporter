package com.lightsource.monitoring.mongo

import com.github.fakemongo.Fongo
import grizzled.slf4j.Logging
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.testinjections.fakedb

object FongoStorage extends MongoStorage(fakedb) with Logging {

  override def findInstallationNameForMeter(meterid: String) = null
  
}