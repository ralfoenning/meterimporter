package com.lightsource.monitoring.meterimport

import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import org.joda.time.DateTime

@RunWith(classOf[JUnitRunner])
class DateTest extends Specification {

  "local date time" should {

//    "work" in {
//
//      val csv = DateTimeFormat.forPattern("HH:mm:ss E dd/MM/yy").parseDateTime("01:02:19 Wed 01/04/2015")
//      val midnightSansTZ = new LocalDateTime(csv.getYear, csv.getMonthOfYear, csv.getDayOfMonth, 0, 0, 0)
//      println(midnightSansTZ)
//
//      val midnightWithTZ = midnightSansTZ.toDateTime(DateTimeZone.UTC)
//      println(midnightWithTZ)
//
//      ok
//    }

    "work with mongo" in {
    	RegisterJodaTimeConversionHelpers()

      val dt = DateTimeFormat.forPattern("HH:mm:ss E dd/MM/yy").parseDateTime("00:02:19 Wed 01/04/2015")
      println(dt)

      println(MongoDBObject("timestamp" -> dt))
      val dt2 = dt.toDateTime(DateTimeZone.UTC)
      println(MongoDBObject("timestamp" -> dt2))

      val ldt : LocalDateTime = DateTimeFormat.forPattern("HH:mm:ss E dd/MM/yy").parseLocalDateTime("00:02:19 Wed 01/04/2015")

      println(ldt)

      println(MongoDBObject("timestamp" -> ldt))

      val ldt2: DateTime = ldt.toDateTime(DateTimeZone.UTC)
      println(MongoDBObject("timestamp" -> ldt2))

      ok
    }
  }
}