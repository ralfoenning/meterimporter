package com.lightsource.monitoring.alerting

import org.joda.time.DateTime
import com.lightsource.monitoring.alerting.AlarmAction.AlarmAction
import com.lightsource.monitoring.alerting.AlarmAction.CloseIfAny
import com.lightsource.monitoring.alerting.AlarmAction.New
import com.lightsource.monitoring.common.MonitoringStorage
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime

  class ActionerWithDay(rep: MonitoringStorage, action: AlarmAction)(val meterid: String, val csvday: DateTime) extends AlarmActioner {

    def _doit() = action match {

      case CloseIfAny => {
        logger.info("closing alarm: " + meterid + "-" + csvday)
        rep.closeAlarm(meterid, csvday)
      }
      case New => {
        logger.info("inserting alarm: " + meterid + "-" + csvday)
        rep.createAlarm(meterid, csvday)
      }
      case _ => {}
    }

    override def toString = action.toString()
  }