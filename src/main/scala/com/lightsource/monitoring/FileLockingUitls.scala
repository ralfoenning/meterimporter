package com.lightsource.monitoring

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.RandomAccessFile

object FileLockingUtils {

  object SimpleARM {
    def apply[T, Q](c: T { def close(): Unit })(f: (T) => Q): Q = {
      try {
        f(c)
      } finally {
        c.close()
      }
    }
  }

  def canBeExclusivelyLocked(file: File): Boolean = {

    def lockAndUnlock(file: File) = {
      val in = new RandomAccessFile(file, "rw")
      try {
        val lock = in.getChannel().lock(0L, Long.MaxValue, false);
        try {
        } finally {
          lock.release()
        }
      } finally {
        in.close()
      }
    }
    try { lockAndUnlock(file); true } catch {
      case e: Exception => {
        e.printStackTrace()
        false
      }
    }
  }

  def isCompletelyWritten(file: File): Boolean = {
    var stream: RandomAccessFile = null
    try {
      stream = new RandomAccessFile(file, "rw")
      return true
    } catch {
      case e: Exception =>
        println("Skipping file " + file.getName() + " for this iteration due it's not completely written")
        return false
    } finally {
      if (stream != null) {
        try {
          stream.close()
        } catch {
          case ie: IOException => println("Exception during closing file " + file.getName())
        }
      }
    }
  }

  def isWriteableAndThereforeCompletelyWritten(file: File): Boolean = {

    val f = (raf: RandomAccessFile) => true
    try { SimpleARM(new RandomAccessFile(file, "rw"))(f) } catch { case e: Exception => false }
  }

}