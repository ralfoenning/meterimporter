package com.lightsource.monitoring

import java.nio.file.Files
import java.nio.file.Path
import grizzled.slf4j.Logging
import java.nio.charset.StandardCharsets
import java.io.IOException
import scala.reflect.runtime.{ universe => ru }

class MeterFileOpening[T]()(implicit tag: ru.TypeTag[T]) extends Logging {

  private[monitoring] def extractListOfTimeSeries(p: Path): Seq[T] = {

    import scala.collection.JavaConversions._

    var count = 5
    while (!FileLockingUtils.canBeExclusivelyLocked(p.toFile()) && count > 0) {
      count = count - 1
      Thread.sleep(200)
    }

    if (!FileLockingUtils.isWriteableAndThereforeCompletelyWritten(p.toFile()))
      throw new IOException("!isWriteableAndThereforeCompletelyWritten: " + p.toFile())

    val res = tag.tpe.toString() match {
      case "String" => Files.readAllLines(p, StandardCharsets.UTF_8).toList.asInstanceOf[Seq[T]]
      case _ => {
        val xmlStraight = new String(Files.readAllBytes(p), StandardCharsets.UTF_8)

        val xmlString = xmlStraight.trim().replaceFirst("^([\\W]+)<", "<") //content-is-not-allowed-in-prolog

        val res = scala.xml.XML.loadString(xmlString)

        val res2 = (res \\ "Chart1_SeriesGroup")

        res2.toList.asInstanceOf[Seq[T]]
      }
    }
    logger.info(res)
    res

  }
}