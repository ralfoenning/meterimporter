package com.lightsource.integrationtests

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.github.fakemongo.Fongo
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.casbah.Imports.JodaDateTimeDoNOk
import com.mongodb.casbah.Imports.mongoQueryStatements
import com.mongodb.casbah.Imports.wrapDBObj
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner
import com.lightsource.testinjections._


@RunWith(classOf[JUnitRunner])
class AlarmIntegrationTest extends Specification {
  
  val noPriorAlarm = Some((m: String, d: LocalDate) => false)
  val existingAlarm = Some((m: String, d: LocalDate) => true)

  val produceAlarmActionsWithoutPriorAlarms = produceAlarmActions(noPriorAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )
  val produceAlarmActionsWithExistingAlarms = produceAlarmActions(existingAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )

  sequential

  "create alarm" should {

    "work low level" in {

      import com.mongodb.casbah.Imports.wrapDBObj

      val alarmscoll = new Fongo("fakemongo").getDB("meterdb").getCollection("alarms")
      alarmscoll.insert(MongoDBObject("_id" -> "idaaa") ++
        ("installation" -> null) ++
        ("description" -> "No Production") ++
        ("meterId" -> "idaaa") ++
        ("from" -> DateTime.now().minusDays(1)) ++
        ("to" -> null))

      alarmscoll.count() must_== 1
      val dtForCasbah = LocalDate.now().toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
      import com.mongodb.casbah.Imports._

      alarmscoll.count(("from" $lte dtForCasbah) ++ ("meterId" -> "idaaa") ++ ("to" -> null)) must_==1
      alarmscoll.count(("from" $lte DateTime.now()) ++ ("meterId" -> "idaaa") ++ ("to" -> null)) must_==1

      ok
    }

    "work with FongoStorage" in {

      FongoStorage.hasOldOngoingAlarm("123", LocalDate.now()) must_== false
      FongoStorage.createAlarm("123", DateTime.now().minusDays(1))
      FongoStorage.hasOldOngoingAlarm("123", LocalDate.now()) must_== true
      FongoStorage.closeAlarm("123", DateTime.now().minusDays(1))
      FongoStorage.hasOldOngoingAlarm("123", LocalDate.now()) must_== false
      ok
    }
  }

  "startgap evaluation" should {

    val startGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 0, 0, 129.765, 202.035, 0, 0, 0, 339.595, 348.19, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that is closed if no prior alarm exists" in {

      FongoStorage.hasOldOngoingAlarm("123", LocalDate.now.plusDays(1)) must_== false

      val produceAlarms = produceAlarmActions(None).curried("gen")("123")(LocalDate.now())(List(DateTime.now()))(startGap)

      produceAlarms.map { _.toString() } must_== List("New", "CloseIfAny")

      FongoStorage.hasOldOngoingAlarm("123", LocalDate.now.plusDays(1)) must_== false

      //      List({ "serverUsed" : "localhost:27017" , "ok" : 1 , "n" : 0}, { "serverUsed" : "localhost:27017" , "ok" : 1 , "n" : 1 , "updatedExisting" : true})
    }
    //    "result in existing alarm that is closed if prior alarm exists" in {
    //      produceAlarmActionsWithExistingAlarms(startGap).map { _.toString() } must_== List("Nothing", "CloseIfAny")
    //    }
  }
  "nogap" should {

    val noGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 348.19, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in no modifications if no prior alarm" in {
      skipped
      produceAlarmActionsWithoutPriorAlarms(noGap).map { _.toString() } must_== List("CloseIfAny", "Nothing")
    }
    "result in existing alarm being closed" in {
      skipped
      produceAlarmActionsWithExistingAlarms(noGap).map { _.toString() } must_== List("CloseIfAny", "Nothing")
    }
  }
  "fullgap evaluation" should {

    val fullGap: List[Double] = List.fill(48)(0.0)

    "result in new alarm that is not closed if no prior alarm exists" in {
      skipped
      produceAlarmActionsWithoutPriorAlarms(fullGap).map { _.toString() } must_== List("New", "Nothing")
      println(produceAlarmActionsWithoutPriorAlarms(fullGap).map { _.doit() })

      MongoStorage.hasOldOngoingAlarm("123", LocalDate.now.plusDays(1)) must_== true

    }
    "result in nothing if prior alarm exists" in {

      skipped
      produceAlarmActions(None).curried("gen")("123")(LocalDate.now())(List(DateTime.now()))(fullGap).map { _.toString() } must_== List("Nothing", "Nothing")
    }
  }
  "endgap evaluation" should {

    val gapAtEnd: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 348.19, 0.0, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that stays open if no prior alarm exists" in {
      skipped
      produceAlarmActionsWithoutPriorAlarms(gapAtEnd).map { _.toString() } must_== List("New", "Nothing")
    }
    "result in existing alarm that stays open if prior alarm exists" in {
      skipped
      produceAlarmActionsWithExistingAlarms(gapAtEnd).map { _.toString() } must_== List("Nothing", "Nothing")
    }
  }
  "midgap evaluation" should {

    val midGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 0.0, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that gets closed if no prior alarm exists" in {
      skipped
      produceAlarmActionsWithoutPriorAlarms(midGap).map { _.toString() } must_== List("New", "CloseIfAny")
    }
    "result in existing alarm that get closed if prior alarm exists" in {
      skipped
      produceAlarmActionsWithExistingAlarms(midGap).map { _.toString() } must_== List("Nothing", "CloseIfAny")
    }
  }

}