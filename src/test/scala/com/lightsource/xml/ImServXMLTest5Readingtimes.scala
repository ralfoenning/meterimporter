package com.lightsource.xml

import java.io.File
import scala.xml.Node
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.specs2.mutable.Specification
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ImServXMLTest5Readingtimes extends Specification {

  "reading imserv xml file" should {

    val onedayPlusGuff =
      <Report Name="RPT_CON2_BasicReportBuilder" >
        <Chart1>
          <Chart1_SeriesGroup_Collection>
            <Chart1_SeriesGroup Label="111 kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
            <Chart1_SeriesGroup Label="kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
          </Chart1_SeriesGroup_Collection>
        </Chart1>
      </Report>
    "work" in {

      println(
        (onedayPlusGuff \\ "Chart1_SeriesGroup").filter(rootn => rootn.attributes.get("Label").map(_.head.text) == Some("111 kWh Import")))
      println(
        (onedayPlusGuff \\ "Chart1_SeriesGroup").filter(rootn => {
          val res : Seq[String] = (rootn \ "@Label").map(_.text)
          res.head == "111 kWh Import"
        }))
      println(
        (onedayPlusGuff \\ "Chart1_SeriesGroup").filter(rootn => (rootn \ "@Label").map(_.text).head == "111 kWh Import"))

      ((onedayPlusGuff \\ "Chart1_SeriesGroup").                                     //all subtrees headed by a SeriesGroup
          filter(rootn => (rootn \ "@Label").map(_.text).head == "111 kWh Import")   //just the subtree with root having that attribute
          \\ "Chart1_CategoryGroup").map(_ \ "@Label").map(_.text).head must_== "22/04/2015 00:30"  //all subtrees...all subtrees...all texts.head of list

    }
  }
}