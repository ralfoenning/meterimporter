package com.lightsource.monitoring.meterimport

import java.io.File
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.joda.time.LocalDate
import com.lightsource.monitoring.MeterFileOpening
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.mongodb.BasicDBObject
import java.nio.file.Files
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.MeterDataHandling
import java.nio.file.Path
import scala.xml.Node
import com.lightsource.monitoring.mongo.FongoStorage

@RunWith(classOf[JUnitRunner])
class ConsumptionCreationTest3 extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "simpleCase subtracting one 20/05 day from two days 19/05-20/05" should {

    "pair the right dates" in {

      val aslf = fileResource("asl-20150520.csv")
      new MeterDataHandling[String, Path].process(aslf.toPath(), new MeterDataCsvFunctions(FongoStorage))
      val imsf = fileResource("imserv2days1meter_241.xml")
      new MeterDataHandling[Node, Path].process(imsf.toPath(), new MeterDataXml(FongoStorage))

      val res = FongoStorage.meterdb.getCollection("readings").find().toArray()
      import scala.collection.JavaConversions._
      println(res.toList.groupBy(_.get("type")).size)

      println(res.toList.filter(_.get("type").asInstanceOf[String] == "generation").size)
      println(res.toList.filterNot(_.get("type").asInstanceOf[String] == "generation").size)

      val timeserieszip = res.toList.filter(_.get("type").asInstanceOf[String] == "generation").zip(res.toList.filter(_.get("type").asInstanceOf[String] == "export"))
      
      println(timeserieszip.size)
      timeserieszip foreach println
      
      
      val timeseriesdiff = timeserieszip.map {

        tup =>
          {
            val gendbo = tup._1
            val expdbo = tup._2

            val genVal = gendbo.get("value").asInstanceOf[Double]
            val expVal = expdbo.get("value").asInstanceOf[Double]

            genVal - expVal

          }
      }
      
      println(timeseriesdiff)


      ok
    }
  }
}