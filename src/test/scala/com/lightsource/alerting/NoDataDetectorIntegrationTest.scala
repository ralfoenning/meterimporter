package com.lightsource.integrationtests

import scala.collection.immutable.Stream.consWrapper
import scala.runtime.ScalaRunTime
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL.double2jvalue
import org.json4s.JsonDSL.jobject2assoc
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.JsonDSL.string2jvalue
import org.json4s.jvalue2monadic
import org.json4s.native.JsonMethods.pretty
import org.json4s.native.JsonMethods.render
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.json4s.native.JsonParser
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import java.net.URLEncoder
import com.lightsource.monitoring.common.{TimeSeries, MongoStorage}
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import org.joda.time.DateTimeZone
import com.github.fakemongo.Fongo
import org.joda.time.LocalTime
import com.lightsource.monitoring.alerting.NoDataDetector
import java.util.Date
import com.lightsource.monitoring.mongo.FongoStorage

@RunWith(classOf[JUnitRunner])
class NoDataDetectorIntegrationTest extends Specification {

    sequential

  "low level interval tests" should {

    "work" in {

      import com.mongodb.casbah.Imports.wrapDBObj
      RegisterJodaTimeConversionHelpers()

      val readings = new Fongo("fakemongo").getDB("meterdb").getCollection("readings")

      readings.insert(MongoDBObject("_id" -> "idaaa") ++
        ("installation" -> null) ++
        ("meterId" -> "idaaa") ++
        ("timestamp" -> DateTime.now().minusHours(1)) ++
        ("value" -> 1.8))

      readings.count() must_== 1
      val lastMidnight = LocalDate.now().toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
      import com.mongodb.casbah.Imports._

      readings.count(("timestamp" $gte lastMidnight) ++ ("timestamp" $lte DateTime.now()) ++ ("meterId" -> "idaaa")) must_== 1
      readings.count(("timestamp" $gte lastMidnight) ++ ("timestamp" $lte DateTime.now().minusHours(2)) ++ ("meterId" -> "idaaa")) must_== 0

      ok
    }

    "checkTimes" should {

      "work" in {

        val ndd = new NoDataDetector()

        val dt = DateTime.parse("2015-05-01T06:00:00Z")
        val e = MongoDBObject("_id" -> "idaaa") ++
          ("installation" -> null) ++
          ("meterId" -> "idaaa") ++
          ("timestamp" -> dt) ++
          ("value" -> 1.8)

//        ndd.timestampOf(e).getMillis must_== dt.getMillis 
        ndd.timesToCheckFor(e) take 2 map(_.toString()) must_==List("2015-05-02T16:00:00.000+01:00", "2015-05-03T16:00:00.000+01:00")
        
        ndd.isPrecededByLargeGap("idxxx", ndd.timesToCheckFor(e).head, FongoStorage) must_==true    //e wasn't written yet
        
        val ld = LocalDate.now()
        FongoStorage.storeTimeseries(TimeSeries("gen", "idxxx", ld, List(dt), List(1.8)))

        ndd.isPrecededByLargeGap("idxxx", ndd.timesToCheckFor(e).head, FongoStorage) must_==true
        ndd.isPrecededByLargeGap("idxxx", ndd.timesToCheckFor(e).tail.head, FongoStorage) must_==true 
        
        FongoStorage.hasOldOngoingAlarm("idxxx", LocalDate.now() ) must_==false
        ndd.createAllAlarmsOn(FongoStorage) 
        FongoStorage.hasOldOngoingAlarm("idxxx", LocalDate.now() ) must_==true
        
        ok
      }
    }
  }
}