package com.lightsource.monitoring

import com.lightsource.monitoring.alerting.AlarmActioner
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.nio.file.Path
import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage}
import com.lightsource.monitoring.alerting.Alerting

trait TimeseriesFunctions2[CONTENTTYPE] {
  
  val rep: MonitoringStorage

  val makeParseableRawMeterData: CONTENTTYPE => Option[CONTENTTYPE]

  val parseSingleMeterData: CONTENTTYPE => TimeSeries

  val storeSingleMeterData = rep.storeTimeseries

  val produceAlarmActions: TimeSeries => List[AlarmActioner] = ts => Alerting(rep).produceAlarmActions(None).tupled(ts.tupled._1, ts.tupled._2, ts.tupled._3, ts.tupled._4, ts.tupled._5)

  val createOtherTimeseriesFrom = (ts: TimeSeries) => List.empty[TimeSeries]

}