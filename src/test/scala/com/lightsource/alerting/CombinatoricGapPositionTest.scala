package com.lightsource.alerting

import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.lightsource.testinjections._
import org.joda.time.DateTime



/**
 * Tests the combination of non-existing/existing alarms and front/middle/end gaps. 
 */
@RunWith(classOf[JUnitRunner])
class CombinatoricGapPositionTest extends Specification {

  val noPriorAlarm = Some((m: String, d: LocalDate) => false)
  val existingAlarm = Some((m: String, d: LocalDate) => true)

  val produceAlarmActionsWithoutPriorAlarms = produceAlarmActions(noPriorAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )
  val produceAlarmActionsWithExistingAlarms = produceAlarmActions(existingAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )
  

  "startgap evaluation" should {
    
//    Alerting.alarmcoll.drop()
	 
    val startGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 0, 0, 129.765, 202.035, 0, 0, 0, 339.595, 348.19, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that is closed if no prior alarm exists" in {
      produceAlarmActionsWithoutPriorAlarms(startGap).map { _.toString() } must_== List("New", "CloseIfAny")
    }
    "result in existing alarm that is closed if prior alarm exists" in {
      produceAlarmActionsWithExistingAlarms(startGap).map { _.toString() } must_== List("Nothing", "CloseIfAny")
    }
  }
  "nogap" should {

    val noGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 348.19, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in no modifications if no prior alarm" in {
      produceAlarmActionsWithoutPriorAlarms(noGap).map { _.toString() } must_== List("CloseIfAny", "Nothing")
    }
    "result in existing alarm being closed" in {
      produceAlarmActionsWithExistingAlarms(noGap).map { _.toString() } must_== List("CloseIfAny", "Nothing")
    }
  }
  "fullgap evaluation" should {

    val fullGap: List[Double] = List.fill(48)(0.0)

    "result in new alarm that is not closed if no prior alarm exists" in {
      produceAlarmActionsWithoutPriorAlarms(fullGap).map { _.toString() } must_== List("New", "Nothing")
    }
    "result in nothing if prior alarm exists" in {
      produceAlarmActionsWithExistingAlarms(fullGap).map { _.toString() } must_== List("Nothing", "Nothing")
    }
  }
  "endgap evaluation" should {

    val gapAtEnd: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 348.19, 0.0, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that stays open if no prior alarm exists" in {
      produceAlarmActionsWithoutPriorAlarms(gapAtEnd).map { _.toString() } must_== List("New", "Nothing")
    }
    "result in existing alarm that stays open if prior alarm exists" in {
      produceAlarmActionsWithExistingAlarms(gapAtEnd).map { _.toString() } must_== List("Nothing", "Nothing")
    }
  }
  "midgap evaluation" should {

    val midGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 0.0, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "result in new alarm that gets closed if no prior alarm exists" in {
      produceAlarmActionsWithoutPriorAlarms(midGap).map { _.toString() } must_== List("New", "CloseIfAny")
    }
    "result in existing alarm that get closed if prior alarm exists" in {
      produceAlarmActionsWithExistingAlarms(midGap).map { _.toString() } must_== List("Nothing", "CloseIfAny")
    }
  }

}