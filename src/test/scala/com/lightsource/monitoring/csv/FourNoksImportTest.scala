package com.lightsource.monitoring.csv

import java.io.File
import java.nio.file.{Files, Path}
import java.util.Date
import com.lightsource.monitoring.{MeterDataCsvFunctions, MeterDataHandling}
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.BasicDBObject
import org.joda.time.{DateTime, DateTimeZone}
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.casbah.commons.MongoDBObject
import com.lightsource.monitoring.FourNoksHandler
import java.nio.charset.StandardCharsets
import scala.collection.JavaConversions._
import scala.collection.immutable.HashSet

@RunWith(classOf[JUnitRunner])
class FourNoksImportTest extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "importing four noks 13 timeseries for single meter" should {

    "work" in {

      FongoStorage.meterdb.getCollection("readings").drop()

      val f = fileResource("4noks-5EA94940-1436125500.json")
      
      FourNoksHandler.process(f.toPath, FongoStorage)
      
//      Files.readAllLines(f.toPath, StandardCharsets.UTF_8).toList map FourNoksHandler.parse      //just the parsing
      
      FongoStorage.countReadings(MongoDBObject()) must_==5434
      
      FongoStorage.meterdb.getCollection("readings").distinct("type").toSet must_== 
        HashSet("generpwr", "generener", "generener-pence", "soldpwr", "soldener", "soldener-pence", 
            "bougpwr", "bougenertot", "bougenertot-pence", "bougenerf1", "bougenerf1-pence", "bougenerf2", "bougenerf2-pence", 
            "conspwr", "consener", "consener-pence", "extalarm1", "extalarm2", "eliosflag")
      
      val dbo = new BasicDBObject()
      FongoStorage.findOneReading(dbo, dbo, dbo).get.toString must_== """{ "_id" : "5EA94940-generpwr-2015-07-05T19:45:00.000Z-generpwr" , "type" : "generpwr" , "meterId" : "5EA94940-generpwr" , "timestamp" : { "$date" : "2015-07-05T19:45:00.000Z"} , "value" : 0.08 , "unit" : "kW"}"""     

      ok
    }
  }
}