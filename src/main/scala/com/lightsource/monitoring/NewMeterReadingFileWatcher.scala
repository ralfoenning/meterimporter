package com.lightsource.monitoring

import java.nio.file.FileSystem
import java.nio.file.LinkOption.NOFOLLOW_LINKS
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchEvent
import java.nio.file.WatchEvent.Kind
import java.nio.file.WatchKey
import java.nio.file.WatchService
import scala.collection.JavaConversions._
import org.joda.time.DateTime
import org.vertx.java.core.json.JsonObject
import org.vertx.scala.platform.Verticle
import com.lightsource.monitoring.configs.meterreadings.FileTypes
import com.lightsource.monitoring.common.{ TimeSeries, MongoStorage }
import grizzled.slf4j.Logger
import java.nio.file.StandardWatchEventKinds
import org.vertx.scala.core.eventbus.Message
import org.vertx.java.core.AsyncResult
import scala.xml.Node

class NewMeterReadingFileWatcher extends Verticle {

  val glogger = Logger[this.type]

  implicit val monitoringStorage = MongoStorage

  System.setProperty(org.slf4j.impl.SimpleLogger.LOG_FILE_KEY, "/tmp/vertxwatcher.log")
  System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_DATE_TIME_KEY, "false")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_LOG_NAME_KEY, "false")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_THREAD_NAME_KEY, "false")

  override def start() = {

    vertx.eventBus.registerHandler("monws-feed", { message: Message[String] =>
      {
        val m = message.body()
        container.logger.info("Received news: " + m)
        val res = org.json4s.native.JsonParser.parse(m)
        container.logger().info(res \\ "shoesize")

      }
    }, { asyncResult: AsyncResult[Void] =>
      println("The mon-ws handler has been registered across the cluster ok? " + asyncResult.succeeded())
    })

    val r = new WatchTask(container.config())
    val thread = new Thread(r)

    thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
      def uncaughtException(th: Thread, ex: Throwable) {
        println("Uncaught exception in watcher thread: " + ex)
        println("Exiting...")
        System.exit(1)
      }
    })
    thread.start()
    container.logger().info("watcher thread started")
    glogger.info("watcher thread started slf4j")

    sys addShutdownHook {
      println("Process is exiting...")
      thread.interrupt()
      Thread.sleep(300)
      println("Done with clean shutdown.")
    }
  }

  /**
   * New path created: Lightsource-A1140_cmdReport20150403010210.csv
   * New path created: class sun.nio.fs.UnixPath
   *
   */
  case class WatchTask(config: JsonObject) extends Runnable {
    override def run() = {

      val defaultwhatToWatch = System.getProperty("user.home") + "/Dropbox/lsm"
      val watchpath = Paths.get(if (config.containsField("watchdir")) config.getString("watchdir") else defaultwhatToWatch)

      val service = watchpath.getFileSystem.newWatchService()
      watchpath.register(service, StandardWatchEventKinds.ENTRY_CREATE)
      println("Watching path: " + watchpath)

      try {
        while (!Thread.currentThread().isInterrupted()) {
          val key = service.take()
          val list = key.pollEvents()

          list.foreach { ev =>
            ev.kind() match {
              case StandardWatchEventKinds.ENTRY_CREATE => {
                glogger.info("New path created: " + ev.context())
                val nameOfNewFile = ev.context().asInstanceOf[Path]
                val extension = nameOfNewFile.toString.replaceAll("^.*\\.(.*)$", "$1").toLowerCase
                glogger.info("content type: " + extension)
                if (!FileTypes(config.toString()).supported.contains(extension)) glogger.info("ignoring...")
                else {
                  val fullPathOfIt = watchpath.resolve(nameOfNewFile)
                  try {
                    glogger.info(s"${DateTime.now()}-processing file ${fullPathOfIt}")

                    MeterFileOpening2.access(fullPathOfIt)

                    val timeseriesSeq: Seq[TimeSeries] = extension match {
                      case "csv" => new MeterDataHandling[String, Path]().process(fullPathOfIt, new MeterDataCsvFunctions(monitoringStorage))
                      case "xml" => new MeterDataHandling[Node, Path]().process(fullPathOfIt, new MeterDataXml(monitoringStorage))
                      case "json" => FourNoksHandler.process(fullPathOfIt, monitoringStorage)
                    }

                    timeseriesSeq foreach println
                    timeseriesSeq foreach (ts => println(ts.toJson))

                    timeseriesSeq.map { ts => vertx.eventBus.publish("meterimport-feed", ts.toJson) }

                    //                    val tfunc = new RealTimeseriesFunctions(monitoringStorage)
                    //                    val listOfNewTimeseries = timeseriesSeq.flatMap { t => tfunc.createOtherTimeseriesFrom(t) }
                    //                    listOfNewTimeseries.map { newtimeseries => tfunc.storeSingleMeterData(newtimeseries) }

                  } catch {
                    case e: Exception => glogger.error(s"Error processing ${nameOfNewFile}\n", e)
                  }

                }
              }
              case _ => glogger.warn(ev.kind().name())
            }
          }
          key.reset()
        }
      } catch {
        case ie: InterruptedException => println("bye from watcher thread!")
        case oe: Exception            => glogger.error("Error at service level", oe)
      } finally {
        println("closing down watch service...")
        service.close()
      }
    }
  }
}