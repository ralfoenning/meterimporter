package com.lightsource.accumulators

import com.lightsource.monitoring.common.TimeSeries
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.mongodb.BasicDBObject
import com.mongodb.DBObject
import org.joda.time.DateTimeZone
import com.mongodb.BasicDBObject
import com.lightsource.monitoring.mongo.FongoStorage

@RunWith(classOf[JUnitRunner])
class AccumulatorsTest extends Specification {
  
  sequential

  val srcLineAsl = """
			Demo9,EML1206003662   ,03:41:35 Wed 11/03/15,10980410,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,
			0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""

  val ld = LocalDate.now()
  val dt = DateTime.parse("2015-03-29T00:30:00.000+01:00").toDateTime(DateTimeZone.UTC)

  "producing day accumulators from string" should {

    "work" in {

      val csvfcts = new MeterDataCsvFunctions(FongoStorage)
      val allrelevantData = csvfcts.parseSingleMeterData(srcLineAsl)
      
      val readingValues = allrelevantData.values
      
      csvfcts.storeSingleMeterData(allrelevantData)
      
      FongoStorage.meterdb.getCollection("totals").find().next().get("value") must_== readingValues.reduce(_+_) 

      ok
    }
    "work with totals (two stores for same day create two totals!)" in {


      FongoStorage.meterdb.getCollection("readings").remove(new BasicDBObject())
      FongoStorage.meterdb.getCollection("totals").remove(new BasicDBObject())

      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt, dt.minusHours(1)), List(1.8, 0.0) ))
      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt.minusHours(1), dt.minusHours(2)), List(1.5, 0.0) ))

      FongoStorage.findOneReading(new BasicDBObject(), new BasicDBObject(), new BasicDBObject()).get.toString() must_==
        """{ "_id" : "id2-2015-03-28T23:30:00.000Z-gen" , "type" : "gen" , "meterId" : "id2" , "timestamp" : { "$date" : "2015-03-28T23:30:00.000Z"} , "value" : 1.8 , "unit" :  null }"""

      FongoStorage.meterdb.getCollection("totals").count() must_== 2

      import scala.collection.JavaConversions._
      FongoStorage.meterdb.getCollection("totals").find(new BasicDBObject(), new BasicDBObject("value", 1)).iterator() foreach println

      FongoStorage.meterdb.getCollection("totals").find(new BasicDBObject()).iterator().map(_.get("value").asInstanceOf[Double]).toSet must_== Set(1.5, 1.8)
      
      println("===")
      val c = FongoStorage.meterdb.getCollection("readings").find() 
      while (c.hasNext()) println(c.next())
      println( FongoStorage.entriesForInInterval("id2", dt.minusDays(1), dt.plusDays(1)) )
      println( FongoStorage.entriesForInInterval("id2", dt, dt) )

      ok
    }
    "no totals for single-time timeseries" in {
    	FongoStorage.meterdb.getCollection("totals").remove(new BasicDBObject())
    	FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt), List(1.8) ))
    	FongoStorage.meterdb.getCollection("totals").count() must_== 0
      
      ok
    }
    

  }

}