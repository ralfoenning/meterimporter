package com.lightsource.monitoring.meterimport

import java.io.File
import java.nio.file.Path
import com.lightsource.monitoring.common.TimeSeries
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.immutable.HashSet
import scala.xml.Node
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.BasicDBObject
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.derived.DerivedConsumptionkwh
import com.lightsource.monitoring.derived.DerivedConsumptionkwhFromExport
import com.lightsource.monitoring.derived.DerivedConsumptionkwhFromGeneration

@RunWith(classOf[JUnitRunner])
class ConsumptionCreationTest9ExportNoGeneration extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "simpleCase subtracting one 20/05 day from two days 19/05-20/05" should {

    "pair the right dates" in {

      FongoStorage.meterdb.getCollection("readings").drop()
      FongoStorage.meterdb.getCollection("totals").drop()

      val imsf = fileResource("imserv2days1meter_241.xml")
      val exportts = new MeterDataHandling[Node, Path].process(imsf.toPath(), new MeterDataXml(FongoStorage))

      FongoStorage.countReadings(new BasicDBObject) must_== 96 * 2
      FongoStorage.meterdb.getCollection("totals").count() must_== 4

      
      
      
      val res = exportts.map(_.toJson)
      val t2 = res.map(TimeSeries.fromJson(_))

      
      
      
      //do the job of the derivedts verticle directly for now
      val tfunc = new DerivedConsumptionkwhFromExport(FongoStorage)
      t2.foreach { ts => tfunc.createOtherTimeseriesFrom(ts).foreach(FongoStorage.storeTimeseries(_)) }

      FongoStorage.countReadings(new BasicDBObject) must_== 96 * 2

      import scala.collection.JavaConversions._
      FongoStorage.meterdb.getCollection("totals").find().toArray().toList foreach println
      FongoStorage.meterdb.getCollection("totals").count() must_== 4

      FongoStorage.meterdb.getCollection("readings").distinct("meterId").toSet must_== HashSet(
        "2394000121241", "2394000121241-export-pence")
      println(FongoStorage.meterdb.getCollection("readings").distinct("meterId"))
      FongoStorage.meterdb.getCollection("readings").distinct("type").toSet must_==
        HashSet("export", "export-pence")
      FongoStorage.meterdb.getCollection("readings").distinct("unit").toSet must_== HashSet("kwh", "pence")

      ok
    }
  }
}