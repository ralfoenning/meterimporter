package com.lightsource.monitoring.configs.meterreadings

import scala.xml.Node

object XmlMeterTypes {

  private val idtype =
    Map(
      "bogus" -> imserv)

  def getMeterLineExtractor(meterId: String) = idtype.get(meterId)
  
  val knownMeterTypes = List(imserv)

  def getTypeOfReading(xmlString: String) : Option[DaySeriesExtractor[Node]] = {

    xmlString match {
      case ls if ls.size > 0 => Some(imserv)
      case _                    => None
    }
  }
}