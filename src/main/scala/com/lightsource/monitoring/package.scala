package com.lightsource

import org.json4s.JsonAST.JString
import org.json4s.JsonAST.JValue
import org.json4s.JNothing
import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.MongoStorage
import com.mongodb.DBCollection

package object monitoring {

  implicit def jvToString(jv: JValue) = jv match {
    case JString(s) => s
    case JNothing   => ""
    case other @ _  => other.toString()
  }
  implicit def String2Double(s: String): java.lang.Double = {
    val str = s match {
      case "" => "0.0"
      case _  => s
    }
    new java.lang.Double(str)
  }

  implicit def liststr2liststrwrapper[T](l: List[T]) = new {
    def select(p: T => Boolean) = l.filter(p)
    def filterOut(p: T => Boolean) = l.filterNot(p)
  }

  def produceAlarmActions = Alerting(MongoStorage).produceAlarmActions

}