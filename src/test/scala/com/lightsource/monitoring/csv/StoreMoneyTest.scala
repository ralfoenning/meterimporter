package com.lightsource.monitoring.csv

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.casbah.Imports.wrapDBObj
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class StoreMoneyTest extends Specification {

  sequential

  "inserting and finding rates" should {

    "work" in {

      FongoStorage.meterdb.getCollection("tariffs").insert(
        MongoDBObject("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 25, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.123"))
      FongoStorage.getRateForTimeAndMeterId(new DateTime(2015, 3, 25, 0, 0, 0).toDateTime(DateTimeZone.UTC), "EML1411033033") must_== Some(0.123)

      FongoStorage.meterdb.getCollection("tariffs").insert(
        MongoDBObject("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 26, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.124"))
      FongoStorage.getRateForTimeAndMeterId(new DateTime(2015, 3, 26, 1, 2, 3).toDateTime(DateTimeZone.UTC), "EML1411033033") must_== Some(0.124)

      val dbo =
        MongoDBObject("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 26, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.124")
      FongoStorage.meterdb.getCollection("tariffs").update(dbo, dbo ++ ("rate" -> "0.125"), true, false)
      FongoStorage.getRateForTimeAndMeterId(new DateTime(2015, 3, 26, 1, 2, 3).toDateTime(DateTimeZone.UTC), "EML1411033033") must_== Some(0.125)

      val dbowithid =
        MongoDBObject("meterId" -> "EML1411033033") ++ ("_id" -> "123") ++
          ("timestamp" -> new DateTime(2015, 3, 26, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.124")
      FongoStorage.meterdb.getCollection("tariffs").insert(dbowithid)
      FongoStorage.meterdb.getCollection("tariffs").update(dbowithid, dbowithid ++ ("rate" -> "0.126") ++ ("_id" -> "123"), true, false)
          
      
      
      ok

    }
  }

}