package com.lightsource.integrationtests

import scala.collection.immutable.Stream.consWrapper
import scala.runtime.ScalaRunTime
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL.double2jvalue
import org.json4s.JsonDSL.jobject2assoc
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.JsonDSL.string2jvalue
import org.json4s.jvalue2monadic
import org.json4s.native.JsonMethods.pretty
import org.json4s.native.JsonMethods.render
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.json4s.native.JsonParser
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import java.net.URLEncoder
import com.lightsource.monitoring.common.{TimeSeries, MongoStorage}
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import com.lightsource.monitoring.mongo.FongoStorage

@RunWith(classOf[JUnitRunner])
class StaleDetectorIntegrationTest extends Specification {
  
//  sequential

  "data older than 33 hours" should {

    "cause alarm" in {

      val dt = DateTime.now().minusHours(34)
      FongoStorage.hasOldOngoingAlarm("idxxx4", dt.toLocalDate()) must_== false

      val ld = LocalDate.now()
      val tuple = TimeSeries("gen", "idxxx4", ld, List(dt), List(1.8) )
      FongoStorage.storeTimeseries(tuple) must_== Some(tuple)

      new com.lightsource.monitoring.alerting.StaleDetector(FongoStorage, Array.empty[String])

      FongoStorage.hasOldOngoingAlarm("idxxx4", dt.toLocalDate()) must_== true

      ok
    }
  }
  "data younger than 33 hours" should {
    
    "not cause alarm" in {

      val dt = DateTime.now().minusHours(32).minusMinutes(59)
      FongoStorage.hasOldOngoingAlarm("idxx", dt.toLocalDate()) must_== false

      val timeseries = TimeSeries("gen", "idxx", LocalDate.now(), List(dt), List(1.8))
      FongoStorage.storeTimeseries(timeseries) must_== Some(timeseries)

      new com.lightsource.monitoring.alerting.StaleDetector(FongoStorage, Array.empty[String])

      FongoStorage.hasOldOngoingAlarm("idxx", dt.toLocalDate()) must_== false

      ok
    }
  }
}