package com.lightsource.monitoring.configs.meterreadings

import org.json4s.JsonAST._
import org.json4s.JsonAST.JNothing
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.json4s.native.JsonParser

case class FileTypes(jsonConfigString: String) {

  val usedUnlessOverriddenInConfig = List("csv", "xml"/*, "json"*/)
  
  
  val config = org.json4s.native.JsonParser.parse(jsonConfigString)

  import scala.collection.JavaConversions._

  val supported = (config \ "supportedfiletypes") match {
    case JNothing => usedUnlessOverriddenInConfig
    case list @ _ => for (JString(s) <- list) yield s
  }
}