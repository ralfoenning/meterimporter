package com.lightsource.monitoring.csv

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.casbah.Imports.wrapDBObj
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class UpdateTest extends Specification {

  sequential

  "inserting and finding rates" should {

    "work" in {
    	FongoStorage.meterdb.getCollection("tariffs").remove(MongoDBObject("_id" -> "123"))

      FongoStorage.meterdb.getCollection("tariffs").insert(
        MongoDBObject("_id" -> "123") ++
          ("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 25, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.123"))

      val dbo =
        MongoDBObject("_id" -> "123") ++
          ("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 25, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.124")
      //      FongoStorage.meterdb.getCollection("tariffs").update(dbo, dbo, true, false)

      //      FongoStorage.meterdb.getCollection("tariffs").remove(dbo)
      //      FongoStorage.meterdb.getCollection("tariffs").insert(dbo)

          FongoStorage.meterdb.getCollection("tariffs").update(MongoDBObject("_id" -> "123"), dbo, true, false)
      FongoStorage.meterdb.getCollection("tariffs").remove(MongoDBObject("_id" -> "123"))
      FongoStorage.meterdb.getCollection("tariffs").insert(dbo)

      ok

    }
  }

}