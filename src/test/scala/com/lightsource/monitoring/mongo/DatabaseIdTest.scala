package com.lightsource.monitoring.mongo

import java.io.File
import java.util.Date
import scala.io.Source
import scala.runtime.ZippedTraversable3.zippedTraversable3ToTraversable
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.common.TimeSeries
import com.mongodb.BasicDBObject
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner
import org.joda.time.LocalDate
import scala.collection.JavaConversions._

@RunWith(classOf[JUnitRunner])
class DatabaseIdTest extends Specification {

  "id" should {

    "contain the timestamp in Zulu" in {

      val ts = TimeSeries("generener", "5EA94940-generener", LocalDate.now(), List(DateTime.now()), List(677.43), None, Some("kWh"), List(), false)

      println(ts)
      
      FongoStorage.storeDayTotal2(ts)

      val m = MongoDBObject()
      val dbo = FongoStorage.meterdb.getCollection("totals").find().toArray().toList.head

      println(dbo.get("_id"))
      val timestamppart = new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC)
      println(timestamppart)

      dbo.get("_id").toString must contain(timestamppart.toString())
      dbo.get("_id").toString must contain("Z")

      ok
    }
  }
}