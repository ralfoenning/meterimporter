package com.lightsource.monitoring

import java.nio.file.Path
import java.nio.file.Files
import java.nio.charset.StandardCharsets
import scala.collection.JavaConversions._

object TimeSeriesExtractorFunctions {

  val csvfileextract = (p: Path) => Files.readAllLines(p, StandardCharsets.UTF_8).toList
  val csvstringextract = (s: String) => s.split("\n").toList
//  val jsonfileextract = (p: Path) => Files.readAllLines(p, StandardCharsets.UTF_8).toList
//  val jsonstringextract = (s: String) => s.split("\n").toList
}

