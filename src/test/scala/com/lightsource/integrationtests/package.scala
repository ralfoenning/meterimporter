package com.lightsource

import org.json4s.JNothing
import org.json4s.JsonAST.JString
import org.json4s.JsonAST.JValue
import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.mongo.FongoStorage
import com.lightsource.monitoring.alerting.Alerting

package object integrationtests {

  implicit def jvToString(jv: JValue) = jv match {
    case JString(s) => s
    case JNothing   => ""
    case other @ _  => other.toString()
  }
  implicit def String2Double(s: String): java.lang.Double = {
    val str = s match {
      case "" => "0.0"
      case _  => s
    }
    new java.lang.Double(str)
  }

  def produceAlarmActions = Alerting(FongoStorage).produceAlarmActions

  def alerting = Alerting(MongoStorage)
}