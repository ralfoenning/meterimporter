package com.lightsource.accumulators

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.TimeSeriesStringToStringFunctions

@RunWith(classOf[JUnitRunner])
class StoreShowingTest extends Specification {

  sequential

  val srcLineAsl0 = """Demo9,EML1206003662   ,03:41:35 Tue 10/03/15,,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""
  val srcLineAsl1 = """Demo9,EML1206003662   ,03:41:35 Wed 11/03/15,10980410,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""
  val srcLineAsl2 = """Demo9,EML1206003662   ,03:41:35 Thu 12/03/15,10980411,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""
  val srcLineAsl3 = """Demo9,EML1206003662   ,03:41:35 Fri 13/03/15,,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""
  "show showing" should {

    "work" in {
      
      FongoStorage.meterdb.getCollection("totals").drop()
      FongoStorage.meterdb.getCollection("readings").drop()
      
//      new MeterDataHandling[String, String]().process(srcLineAsl0, new TimeSeriesStringToStringFunctions(FongoStorage))

      new MeterDataHandling[String, String]().process(srcLineAsl1, new TimeSeriesStringToStringFunctions(FongoStorage))
      val sumfirstday = FongoStorage.meterdb.getCollection("totals").find(MongoDBObject("type" -> "dayTotal-generation")).next().get("value")
      println (sumfirstday)
      val showingfirstday = FongoStorage.meterdb.getCollection("readings").find(MongoDBObject("type" -> "generation-showing")).next().get("value")
      println(showingfirstday)
      new MeterDataHandling[String, String]().process(srcLineAsl2, new TimeSeriesStringToStringFunctions(FongoStorage))
      val sum2ndday = FongoStorage.meterdb.getCollection("totals").find(MongoDBObject("type" -> "dayTotal-generation")).next().get("value")
      println (sum2ndday)
      val showing2ndday = FongoStorage.meterdb.getCollection("readings").findOne(
          MongoDBObject("type" -> "generation-showing"), 
          MongoDBObject(), 
          MongoDBObject("timestamp" -> -1) 
          ).get("value")
      println(showing2ndday)
      new MeterDataHandling[String, String]().process(srcLineAsl3, new TimeSeriesStringToStringFunctions(FongoStorage))
      FongoStorage.meterdb.getCollection("readings").findOne(
          MongoDBObject("type" -> "generation-showing"), 
          MongoDBObject(), 
          MongoDBObject("timestamp" -> -1) 
          ).get("value").
          toString() must_== "10980426"
          
          
      ok
    }

  }

}