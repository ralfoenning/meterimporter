package com.lightsource.monitoring.derived

import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage}
import grizzled.slf4j.Logging

class DerivedConsumptionkwh(rep: MonitoringStorage) extends DerivedTimeseries with Logging {

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries] = thistimeseries => {

    val exporttimeseries = thistimeseries
    val exportmid = thistimeseries.mid
    val generationmid = rep.findPostProcessingTimeseries(exportmid)

    val subsettimesbyexportkwh = 
        (exporttimeseries.times.zip(exporttimeseries.values)).filterNot { t => rep.entriesForInInterval(generationmid, t._1, t._1).isEmpty } 

    val subsettimesbyconsumptionkwh = 
subsettimesbyexportkwh.map { t =>
      {
        val dt = t._1
        val exportval = t._2

        val consumptionval = rep.entriesForInInterval(generationmid, dt, dt).map { other => other.get("value").asInstanceOf[Double] - exportval }.map { num => Math.abs(num) }.head
        (dt, consumptionval)
      }
    }

    val consumptionkwhTs = TimeSeries("consumption", generationmid + "-" + exporttimeseries.mid, 
        exporttimeseries.date, subsettimesbyexportkwh.map(_._1), subsettimesbyconsumptionkwh.map(_._2), None, Some("kwh"))

    //    val consumptionkwhTs2 = TimeSeries("consumption kwh", generationmid + "-" + exporttimeseries.mid + "-kwh", exporttimeseries.date, subsettimesbyconsumptionkwh.unzip._1, subsettimesbyconsumptionkwh.unzip._2)

    consumptionkwhTs :: Nil

  }

  //=============derived version 2===============

  trait Op[T] { def eval: T }
  trait NameOp extends Op[String]
  trait IdOp extends Op[String]
  trait ValueOp extends Op[Number]
  case class ConsumptionNameOp(args: String*) extends NameOp { def eval: String = "consumption" }
  case class ConsumptionIdOp(args: String*) extends IdOp { def eval: String = args.mkString("-") }
  case class GenerationMinusExportOp(genVal: Double, expVal: Double) extends ValueOp { def eval: Number = genVal - expVal }

  val createDerived: List[TimeSeries] => (NameOp, IdOp, ValueOp) => TimeSeries = tslist => (nop, iop, vop) => {

    tslist match {
      case List(ts1, ts2) => {

        def inboth[T](l1: List[T], l2: List[T]): List[T] = {
          val desc = List(l1, l2).sortBy(_.size)
          desc(0).diff(desc(0).diff(desc(1)))
        }

      }
    }

    null
  }
}