import org.vertx.scala.core.Vertx
import org.vertx.scala.core.logging.Logger
import org.vertx.scala.platform.Verticle
import org.vertx.java.core.AsyncResult
import org.vertx.scala.core.streams.Pump
import org.vertx.scala.core.net.NetSocket
import org.vertx.scala.core.json.Json

class EchoServer extends Verticle {

container.deployModule("io.vertx~mod-mongo-persistor~2.0.0-final", Json.obj(), 1, { err: AsyncResult[String] =>
  if (err.succeeded()) {
    container.logger.info("hmm")
  } else {
    err.cause().printStackTrace()
  }
})

//container.logger.info("double hmm")


vertx.createNetServer.connectHandler({ socket: NetSocket =>
  container.logger().info("logger test")
  Pump.createPump(socket, socket).start()
}).listen(8086)

}