package com.lightsource.monitoring.meterimport

import java.io.File
import java.nio.file.Path
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.immutable.HashSet
import scala.xml.Node
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.BasicDBObject
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.derived.DerivedConsumptionkwh

@RunWith(classOf[JUnitRunner])
class ConsumptionCreationTest5 extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "simpleCase subtracting one 20/05 day from two days 19/05-20/05" should {

    "pair the right dates" in {
      
      FongoStorage.meterdb.getCollection("readings").drop()
      FongoStorage.meterdb.getCollection("totals").drop()

      val aslf = fileResource("asl-20150520.csv")
      new MeterDataHandling[String, Path]().process(aslf.toPath(), new MeterDataCsvFunctions(FongoStorage))
      val imsf = fileResource("imserv2days1meter_241.xml")
      val t = new MeterDataHandling[Node, Path].process(imsf.toPath(), new MeterDataXml(FongoStorage))
      
      
      //do the job of the derivedts verticle directly for now
      val derivedlist = t.flatMap(new DerivedConsumptionkwh(FongoStorage).createOtherTimeseriesFrom(_))
      t.foreach { ts => derivedlist.foreach(FongoStorage.storeTimeseries(_)) }
      

      FongoStorage.countReadings(new BasicDBObject) must_==385
      
      import scala.collection.JavaConversions._
      FongoStorage.meterdb.getCollection("totals").find().toArray().toList foreach println
      FongoStorage.meterdb.getCollection("totals").count() must_==8
      
      FongoStorage.meterdb.getCollection("readings").distinct("type").toSet must_== 
        HashSet("generation-showing", "generation", "export", "consumption", "consumption-pence", "export-pence", "generation-pence")
      FongoStorage.meterdb.getCollection("readings").distinct("unit").toSet must_== HashSet("kwh", "pence")

      ok  
    }
  }
}