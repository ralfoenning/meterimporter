package com.lightsource.xml

import java.nio.file.Paths
import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.configs.meterreadings.imserv
import com.lightsource.monitoring.mongo.FongoStorage
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MeterDataXmlFunctionsTest extends Specification {

  sequential
  
  "getting list of timeseries - one per meter -" should {
    
    val uri = this.getClass().getResource("/" + "imserv1day2meters_241-232.xml").toURI()
    val p = Paths.get(uri)

    "work" in {

      val nodes = new MeterDataXml(FongoStorage).extractListOfTimeSeries(p)
      
      nodes.size must_==2
      nodes.head.flatMap(_ \ "@Label").size must_== 1
      nodes.tail.head.flatMap(_ \ "@Label").size must_== 1
      
      nodes.head.flatMap(_ \ "@Label").map(_.toString()).head must beEqualTo(nodes.tail.head.flatMap(_ \ "@Label").map(_.toString()).head ) not
      
      ok
    }
  }

  """obtaining meterid from 
    <Chart1><Chart1_SeriesGroup_Collection><Chart1_SeriesGroup Label="kWh Import">...<Chart1_SeriesGroup Label="214669932 kWh Import">""" should {

    val uri = this.getClass().getResource("/" + "imserv1day2meters_241-232.xml").toURI()
    val p = Paths.get(uri)

    "work" in {

      val nodes = new MeterDataXml(FongoStorage).extractListOfTimeSeries(p)

      nodes.flatMap(_ \ "@Label").map(_.toString()) must_== Seq("2394000121241 kWh Export", "2394000121232 kWh Export")

      nodes.map { n => imserv.meterId(n) } must_== Seq("2394000121241", "2394000121232")
      nodes.map { n => imserv.values(n) } map println
      nodes.map { n => imserv.readingTimes(n) } map println
      nodes.map { n => imserv.date(n) } must_== List(new LocalDate(2015,5,19), new LocalDate(2015,5,19))
      ok
    }
  }
} 