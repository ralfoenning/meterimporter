package com.lightsource.monitoring.mongo

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.lightsource.testinjections._

@RunWith(classOf[JUnitRunner])
class AlarmIdempotencyIntegrationTest extends Specification {

  val noPriorAlarm = Some((m: String, d: LocalDate) => false)
  val existingAlarm = Some((m: String, d: LocalDate) => true)

  val produceAlarmActionsWithoutPriorAlarms = produceAlarmActions(noPriorAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )
  val produceAlarmActionsWithExistingAlarms = produceAlarmActions(existingAlarm).curried("gen")("123")(LocalDate.now())(List(DateTime.now()) )

  sequential

  "removing/readding reading with gap in the past" should {

//    Alerting.alarmcoll.drop()

    val startGap: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1.799, 16.507, 29.3595, 0, 0, 129.765, 202.035, 0, 0, 0, 339.595, 348.19, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    "not close newer ongoing alarms " in {
      skipped
    }
  }
  "readding reading with endgap" should {
    "not overwrite existing or insert new alarm" in {

      ok
    }
  }

}