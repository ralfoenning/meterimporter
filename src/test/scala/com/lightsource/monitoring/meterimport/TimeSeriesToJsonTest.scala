package com.lightsource.monitoring.meterimport

import com.lightsource.monitoring.common.TimeSeries
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.joda.time.DateTimeZone

@RunWith(classOf[JUnitRunner])
class TimeseriesToJsonTest extends Specification {
  
  sequential

  "convert to json and back" should {
    
    val ld = LocalDate.now()
    val date = DateTime.now()

    "converts to Z-Dates" in {

      val origTimeseries = TimeSeries("generation", "123", ld, List(date), List(1.5))
      val json = origTimeseries.toJson
      val reconstructedts = TimeSeries.fromJson(json)

      println(origTimeseries)
      println(json)
      println(reconstructedts)
      
      val expected = TimeSeries("generation", "123", ld, List(date.toDateTime(DateTimeZone.UTC)), List(1.5))

      reconstructedts must_== expected

      ok
    }

    "work with Z-dates" in {

      val origTimeseries = TimeSeries("generation", "123", LocalDate.now(), List(new DateTime("2015-05-18T00:30:00.000Z").toDateTime(DateTimeZone.UTC)), List(1.5))
      val json = origTimeseries.toJson
      val reconstructedts = TimeSeries.fromJson(json)

      println(origTimeseries)
      println(json)
      println(reconstructedts)
      
      reconstructedts.times.head.toString() must_== "2015-05-18T00:30:00.000Z"

      ok
    }
  }
}