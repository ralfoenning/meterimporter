package com.lightsource.xml

import java.io.File
import scala.xml.Node
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.specs2.mutable.Specification
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ImServXMLTest3Values extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())
  def streamResource(resourcePath: String) = this.getClass().getResourceAsStream("/" + resourcePath)

  "reading imserv xml file" should {

    val onedayPlusGuff =
      <Report Name="RPT_CON2_BasicReportBuilder" Textbox22="***** No Data Available for the selected report parameters *****" Textbox76="Report displays data in Time Period End" xmlns="RPT_CON2_BasicReportBuilder" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="RPT_CON2_BasicReportBuilder http://edvsql02lsc:8080/ReportServer?%2FEDV_Reports%2FBasicReportBuilder%2FRPT_CON2_BasicReportBuilder&amp;rs%3AFormat=XML&amp;rs%3ASnapshot%3Aisnull=True&amp;rc%3ASchema=True">
        <Tablix5>
          <Textbox33 Company_Name="Lightsource Midscale Limited" Textbox33="Company Name"/>
          <Textbox35 Measure_Type="Electricity" Textbox35="Measure Type"/>
          <Textbox263 MeasureLevelType_Name="Multiple" Textbox263="Measure Level Type"/>
          <Textbox37 MeasurementQuantity_Name="Consumption" Textbox37="Measure Quantity Converted To"/>
          <Textbox39 Measurementunit_Name="kWh" Textbox39="Measurement Unit Converted To"/>
        </Tablix5>
        <Tablix8>
          <Textbox41 Textbox41="Report At" Textbox42="Measure"/>
          <Textbox2 Textbox2="Aggregate Level" Textbox20="30 (minutes)"/>
          <Textbox249 Textbox249="Start Date" Textbox250="22/04/2015"/>
          <Textbox251 Textbox251="End Date" Textbox252="23/04/2015"/>
          <Textbox235 Textbox235="Transaction Timestamp" Textbox236="29/04/2015 19:04:00"/>
        </Tablix8>
        <Tablix6>
          <Textbox43 Textbox43="Portfolio Item 1" Textbox48="214669932 kWh Import"/>
          <Textbox44 Textbox44="Portfolio Item 2" Textbox49="Holme Mills POC 1 Import"/>
          <Textbox45 Textbox45="Portfolio Item 2"/>
          <Textbox46 Textbox46="Portfolio Item 4"/>
          <Textbox47 Textbox47="Portfolio Item 5"/>
        </Tablix6>
        <Tablix7>
          <Textbox53 Textbox53="Portfolio Item 6"/>
          <Textbox54 Textbox54="Portfolio Item 7"/>
          <Textbox55 Textbox55="Portfolio Item 8"/>
          <Textbox56 Textbox56="Portfolio Item 9"/>
          <Textbox57 Textbox57="Portfolio Item 10"/>
        </Tablix7>
        <Chart1>
          <Chart1_SeriesGroup_Collection>
            <Chart1_SeriesGroup Label="111 kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
            <Chart1_SeriesGroup Label="kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 01:00">
                  <Value Y="1.9"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 01:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 02:00">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 02:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 03:00">
                  <Value Y="2"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 03:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 04:00">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 04:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 05:00">
                  <Value Y="3.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 05:30">
                  <Value Y="8.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 06:00">
                  <Value Y="9.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 06:30">
                  <Value Y="9.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 07:00">
                  <Value Y="4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 07:30">
                  <Value Y="2.1"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 08:00">
                  <Value Y="0.3"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 08:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 09:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 09:30">
                  <Value Y="0.1"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 10:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 10:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 11:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 11:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 12:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 12:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 13:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 13:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 14:00">
                  <Value Y="0.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 14:30">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 15:00">
                  <Value Y="0"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 15:30">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 16:00">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 16:30">
                  <Value Y="0.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 17:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 17:30">
                  <Value Y="2.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 18:00">
                  <Value Y="6.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 18:30">
                  <Value Y="7.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 19:00">
                  <Value Y="7.3"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 19:30">
                  <Value Y="0.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 20:00">
                  <Value Y="1.4"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 20:30">
                  <Value Y="1.7"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 21:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 21:30">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 22:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 22:30">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:30">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 00:00">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
          </Chart1_SeriesGroup_Collection>
        </Chart1>
        <Tablix1>
          <Details2_Collection>
            <Details2>
              <Tablix2 Textbox11="Measurement Unit" Textbox17="Measure" Textbox19="Reading Date" Textbox5="Company Name" Textbox65="Parent Name" Textbox66="Total" Textbox7="Industry Reference Number" Textbox88="Total" Textbox9="Measure Asset Name" Textbox90="102.4">
                <Time_Interval_Collection>
                  <Time_Interval Time_Interval="TP End 
00:30"/>
                  <Time_Interval Time_Interval="TP End 
01:00"/>
                  <Time_Interval Time_Interval="TP End 
01:30"/>
                  <Time_Interval Time_Interval="TP End 
11:00"/>
                  <Time_Interval Time_Interval="TP End 
11:30"/>
                  <Time_Interval Time_Interval="TP End 
12:00"/>
                </Time_Interval_Collection>
                <Details_Collection>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="kWh Import" Parent_Name="2380000845563" READING_VALUE3="53.8" Reading_Datetime="22/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="1.8"/>
                      <Time_Interval READING_VALUE="1.9"/>
                      <Time_Interval READING_VALUE="1.8"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="kWh Import" Parent_Name="2380000845563" READING_VALUE3="48.6" Reading_Datetime="23/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="1.6"/>
                      <Time_Interval READING_VALUE="1.6"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="214669932 kWh Import" Parent_Name="2380000845563" READING_VALUE3="-" Reading_Datetime="22/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="214669932 kWh Import" Parent_Name="2380000845563" READING_VALUE3="-" Reading_Datetime="23/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                    </Time_Interval_Collection>
                  </Details>
                </Details_Collection>
                <Time_Interval_Collection_1>
                  <Time_Interval READING_VALUE1="3.4"/>
                  <Time_Interval READING_VALUE1="3.5"/>
                  <Time_Interval READING_VALUE1="0"/>
                  <Time_Interval READING_VALUE1="0"/>
                  <Time_Interval READING_VALUE1="0"/>
                </Time_Interval_Collection_1>
                <Time_Interval_Collection_2>
                  <Time_Interval/>
                  <Time_Interval/>
                  <Time_Interval/>
                  <Time_Interval/>
                  <Time_Interval/>
                </Time_Interval_Collection_2>
              </Tablix2>
            </Details2>
            <Details2>
              <Tablix2 Textbox11="Measurement Unit" Textbox17="Measure" Textbox19="Reading Date" Textbox5="Company Name" Textbox65="Parent Name" Textbox66="Total" Textbox7="Industry Reference Number" Textbox88="Total" Textbox9="Measure Asset Name" Textbox90="64.5">
                <Time_Interval_Collection>
                  <Time_Interval Time_Interval="TP End 
12:30"/>
                  <Time_Interval Time_Interval="TP End 
13:00"/>
                  <Time_Interval Time_Interval="TP End 
23:00"/>
                  <Time_Interval Time_Interval="TP End 
23:30"/>
                  <Time_Interval Time_Interval="TP End 
00:00"/>
                </Time_Interval_Collection>
                <Details_Collection>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="kWh Import" Parent_Name="2380000845563" READING_VALUE3="42.3" Reading_Datetime="22/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="1.6"/>
                      <Time_Interval READING_VALUE="1.6"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="kWh Import" Parent_Name="2380000845563" READING_VALUE3="22.2" Reading_Datetime="23/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="0"/>
                      <Time_Interval READING_VALUE="2"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="214669932 kWh Import" Parent_Name="2380000845563" READING_VALUE3="-" Reading_Datetime="22/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                    </Time_Interval_Collection>
                  </Details>
                  <Details COMPANY_NAME="Lightsource Midscale Limited" ItemName="214669932 kWh Import" Parent_Name="2380000845563" READING_VALUE3="-" Reading_Datetime="23/04/2015" column1="2380000845563" column2="214669932" column3="kWh">
                    <Time_Interval_Collection>
                      <Time_Interval READING_VALUE="-"/>
                      <Time_Interval READING_VALUE="-"/>
                    </Time_Interval_Collection>
                  </Details>
                </Details_Collection>
                <Time_Interval_Collection>
                  <Time_Interval READING_VALUE1="0"/>
                  <Time_Interval READING_VALUE1="0"/>
                  <Time_Interval READING_VALUE1="3.4"/>
                  <Time_Interval READING_VALUE1="3.4"/>
                  <Time_Interval READING_VALUE1="3.6"/>
                </Time_Interval_Collection>
                <Time_Interval_Collection>
                  <Time_Interval/>
                  <Time_Interval/>
                  <Time_Interval/>
                </Time_Interval_Collection>
              </Tablix2>
            </Details2>
          </Details2_Collection>
        </Tablix1>
        <Tablix3>
          <Details3_Collection>
            <Details3>
              <Tablix4 Textbox10="Measure Asset Name" Textbox119="Total" Textbox121="102.4" Textbox18="Measure" Textbox21="Measurement Unit" Textbox6="Company Name" Textbox64="Total" Textbox71="Parent Name" Textbox8="Industry Reference Number">
                <Time_Interval2_Collection>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                </Time_Interval2_Collection>
                <Details4_Collection>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="kWh Import" Parent_Name1="2380000845563" READING_VALUE4="53.8" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="1.8"/>
                      <Time_Interval2 READING_VALUE2="1.9"/>
                      <Time_Interval2 READING_VALUE2="0"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="kWh Import" Parent_Name1="2380000845563" READING_VALUE4="48.6" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="1.6"/>
                      <Time_Interval2 READING_VALUE2="1.6"/>
                      <Time_Interval2 READING_VALUE2="0"/>
                      <Time_Interval2 READING_VALUE2="0"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="214669932 kWh Import" Parent_Name1="2380000845563" READING_VALUE4="-" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="214669932 kWh Import" Parent_Name1="2380000845563" READING_VALUE4="-" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                    </Time_Interval2_Collection>
                  </Details4>
                </Details4_Collection>
                <Time_Interval2_Collection_1>
                  <Time_Interval2 READING_VALUE5="3.4"/>
                  <Time_Interval2 READING_VALUE5="3.5"/>
                  <Time_Interval2 READING_VALUE5="0"/>
                  <Time_Interval2 READING_VALUE5="0"/>
                </Time_Interval2_Collection_1>
                <Time_Interval2_Collection_2>
                  <Time_Interval2/>
                  <Time_Interval2/>
                  <Time_Interval2/>
                </Time_Interval2_Collection_2>
              </Tablix4>
            </Details3>
            <Details3>
              <Tablix4 Textbox10="Measure Asset Name" Textbox119="Total" Textbox121="64.5" Textbox18="Measure" Textbox21="Measurement Unit" Textbox6="Company Name" Textbox64="Total" Textbox71="Parent Name" Textbox8="Industry Reference Number">
                <Time_Interval2_Collection>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                  <Time_Interval2 Time_Interval2="22/04/2015"/>
                </Time_Interval2_Collection>
                <Details4_Collection>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="kWh Import" Parent_Name1="2380000845563" READING_VALUE4="42.3" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="0"/>
                      <Time_Interval2 READING_VALUE2="0"/>
                      <Time_Interval2 READING_VALUE2="1.6"/>
                      <Time_Interval2 READING_VALUE2="1.6"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="kWh Import" Parent_Name1="2380000845563" READING_VALUE4="22.2" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="0"/>
                      <Time_Interval2 READING_VALUE2="0"/>
                      <Time_Interval2 READING_VALUE2="2"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="214669932 kWh Import" Parent_Name1="2380000845563" READING_VALUE4="-" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                    </Time_Interval2_Collection>
                  </Details4>
                  <Details4 COMPANY_NAME2="Lightsource Midscale Limited" ItemName2="214669932 kWh Import" Parent_Name1="2380000845563" READING_VALUE4="-" column6="2380000845563" column7="214669932" column8="kWh">
                    <Time_Interval2_Collection>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                      <Time_Interval2 READING_VALUE2="-"/>
                    </Time_Interval2_Collection>
                  </Details4>
                </Details4_Collection>
                <Time_Interval2_Collection>
                  <Time_Interval2 READING_VALUE5="0"/>
                  <Time_Interval2 READING_VALUE5="3.4"/>
                  <Time_Interval2 READING_VALUE5="3.4"/>
                  ß
                  <Time_Interval2 READING_VALUE5="3.6"/>
                </Time_Interval2_Collection>
                <Time_Interval2_Collection>
                  <Time_Interval2/>
                  <Time_Interval2/>
                  <Time_Interval2/>
                </Time_Interval2_Collection>
              </Tablix4>
            </Details3>
          </Details3_Collection>
        </Tablix3>
      </Report>
    "work" in {

      println("===ONE DAY===")

      println("all times: " + (onedayPlusGuff \\ "Chart1_CategoryGroup").collect { case node: Node => node.attributes.get("Label") })
      println("all values:" + (onedayPlusGuff \\ "Value").collect { case node: Node => node.attributes.get("Y") })

      println("all times/values: " + (onedayPlusGuff \\ "Chart1_CategoryGroup").collect {
        case groupnode: Node => {
          (groupnode.attributes.get("Label"), (groupnode \\ "Value").head.attributes.get("Y"))
        }
      })

      println(
        (onedayPlusGuff \\ "Chart1_SeriesGroup").filter(n => n.attributes.get("Label") == "kWh Import"))
      //.map(_ \\ "Chart1_CategoryGroup").map(_ \ "@Label").map(_.text).head must_== "22/04/2015 00:30"
      (onedayPlusGuff \\ "Chart1_CategoryGroup").map(_ \\ "Value").map(_ \ "@Y").map(_.text).head must_== "1.8"

      ok
    }
  }
}