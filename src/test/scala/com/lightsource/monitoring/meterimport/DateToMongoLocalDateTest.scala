package com.lightsource.monitoring.meterimport

import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import org.joda.time.DateTime
import com.lightsource.monitoring.configs.meterreadings.common
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaLocalDateTimeConversionHelpers

@RunWith(classOf[JUnitRunner])
class DateToMongoTest extends Specification {

  "local date time" should {

    "work with mongo" in {
//    	RegisterJodaLocalDateTimeConversionHelpers()
    	RegisterJodaTimeConversionHelpers()

      val ldt : LocalDateTime = DateTimeFormat.forPattern("HH:mm:ss E dd/MM/yy").parseLocalDateTime("00:02:19 Wed 01/04/2015")
      
      val dtlist = common.calculateReadingTimes((s: String) => ldt.toLocalDate())("")   
      val ldt2: DateTime = dtlist(0).toDateTime(DateTimeZone.UTC)
      
      println(MongoDBObject("timestamp" -> dtlist(0)))
      println(MongoDBObject("timestamp" -> ldt2))


      println(MongoDBObject("timestamp" -> dtlist(0)))
      println(MongoDBObject("timestamp" -> ldt2))
      
      
      ok
    }
  }
}