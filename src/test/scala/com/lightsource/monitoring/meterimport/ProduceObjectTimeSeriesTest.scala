package com.lightsource.monitoring.meterimport

import com.lightsource.monitoring.common.TimeSeries
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.lightsource.monitoring.mongo.FongoStorage
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ProduceObjectTimeSeriesTest extends Specification {

  "producing objects from the data in a meter file" should {

    "work" in {
      
      val srcLineAsl = """
        Demo9,EML1206003662   ,03:41:35 Wed 11/03/15,10980410,11804,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0.015,0,0.153,0,0.366,0,0.692,0,0.909,0,1.065,0,1.199,0,1.18,0,1.361,0,1.377,0,1.362,0,1.255,0,1.365,0,0.95,0,1.007,0,
        0.626,0,0.378,0,0.227,0,0.12,0,0.097,0,0.042,0,0.007,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""
      
      val ts: TimeSeries = new MeterDataCsvFunctions(FongoStorage).parseSingleMeterData(srcLineAsl)
      
      val dbojects = FongoStorage.produceObjectTimeSeries(ts)
      
      dbojects(24).get("timestamp").toString() must_== "2015-03-11T12:30:00.000Z"
      dbojects(24).get("value") must_== 1.362
      
      ok
    }
  }  
}