package com.lightsource.monitoring.csv

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ParsingTest extends Specification {

  "parsing ASL meter reading on a low level" should {

    "work" in {

      val srcLine = """Demo21,EML1411033033   ,04:08:16 Wed 25/03/15,2640319,5198,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0,0,0.001,0,
        0.001,0,0.001,0.005,0.001,0.124,0,0.338,0,0.633,0,0.877,0,1.101,0,1.295,0,1.344,0,1.457,0,1.247,0,1.589,0,1.501,0,1.752,0,1.728,0,1.431,0,1.593,0,1.425,0,
        0.885,0,0.85,0,0.392,0,0.136,0,0.089,0,0.018,0,
        0,0.002,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0"""
      
//      println(srcLine.split(",").length)
//      println(MeterFileHandling.getTypeOfReading(srcLine))

      import com.lightsource.monitoring.configs.meterreadings.{aslgeneration => aslg}
      
      aslg.readingTimes(srcLine)(0).toString() must_== "2015-03-25T00:30:00.000Z"
      aslg.readingTimes(srcLine).last.toString() must_== "2015-03-26T00:00:00.000Z"

      aslg.values(srcLine).size must_== 48
      aslg.values(srcLine).slice(14, 18) must_== List("0.005","0.124","0.338","0.633")
      
      ok
      
    }
  }

}