package com.lightsource.xml

import java.io.File
import java.nio.file.Path
import scala.xml.Node
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.BasicDBObject
import org.specs2.runner.JUnitRunner
import scala.collection.immutable.HashSet

@RunWith(classOf[JUnitRunner])
class ImservImportTest extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "importing imserv xml file into mongodb" should {

    "work" in {

      FongoStorage.meterdb.getCollection("readings").drop()
      FongoStorage.meterdb.getCollection("totals").drop()
      FongoStorage.meterdb.getCollection("alarms").drop()

      val f = fileResource("imserv1day2meters_241-232.xml")
      new MeterDataHandling[Node, Path].process(f.toPath(), new MeterDataXml(FongoStorage))
      
      FongoStorage.allknownMeterids().toSet  must_== HashSet("2394000121241", "2394000121232", "2394000121241-export-pence", "2394000121232-export-pence")

      FongoStorage.countReadings(new BasicDBObject()) must_== 96 + 96 //kwh + pence
      
      val dbo = new BasicDBObject()
      FongoStorage.findOneReading(dbo, dbo, dbo).map(_.get("type")) must_==Some("export")

      FongoStorage.meterdb.getCollection("alarms").count() must_==0
      FongoStorage.meterdb.getCollection("totals").count() must_==4
      
      ok
    }
  }
}