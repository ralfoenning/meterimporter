package com.lightsource

import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.mongo.FongoStorage
import com.github.fakemongo.Fongo

package object testinjections {

  def produceAlarmActions = Alerting(FongoStorage).produceAlarmActions

  def alerting = Alerting(MongoStorage)

  val fakedb = new Fongo("fakemongo").getDB("meterdb")

}