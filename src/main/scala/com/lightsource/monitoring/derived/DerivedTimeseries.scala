package com.lightsource.monitoring.derived

import com.lightsource.monitoring.common.TimeSeries

trait DerivedTimeseries {

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries]
}