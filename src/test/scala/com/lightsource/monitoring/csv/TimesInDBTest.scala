package com.lightsource.monitoring.csv

import java.util.Date
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.TimeSeriesStringToStringFunctions
import com.lightsource.monitoring.configs.meterreadings.{aslgeneration => aslg}
import com.lightsource.monitoring.mongo.FongoStorage
import org.specs2.runner.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class TimesInDBTest extends Specification {
  
  sequential

  "getting the times to the database" should {

    "work with dates in winter" in {
      
      FongoStorage.meterdb.getCollection("readings").drop()

      val srcLine = """Demo21,EML1411033033   ,04:08:16 Wed 25/03/15,2640319,5198,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0,0,0.001,0,        0.001,0,0.001,0.005,0.001,0.124,0,0.338,0,0.633,0,0.877,0,1.101,0,1.295,0,1.344,0,1.457,0,1.247,0,1.589,0,1.501,0,1.752,0,1.728,0,1.431,0,1.593,0,1.425,0,        0.885,0,0.85,0,0.392,0,0.136,0,0.089,0,0.018,0,        0,0.002,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0"""

      import com.lightsource.monitoring.configs.meterreadings.{aslgeneration => aslg}
      aslg.readingTimes(srcLine)(0).toString() must_== "2015-03-25T00:30:00.000Z"
      aslg.readingTimes(srcLine).last.toString() must_== "2015-03-26T00:00:00.000Z"

      new MeterDataHandling[String, String]().process(srcLine, new TimeSeriesStringToStringFunctions(FongoStorage))

      println(FongoStorage.meterdb.getCollection("readings").find().next())

      new DateTime(
        FongoStorage.meterdb.getCollection("readings").find().next().get("timestamp").asInstanceOf[Date]).
        toDateTime(DateTimeZone.UTC).
        toString() must_== "2015-03-25T00:30:00.000Z"

      ok

    }
    "work with dates in summer" in {
      
      FongoStorage.meterdb.getCollection("readings").drop()

      val srcLine = """A508100034,14172974,01:03:12 Mon 18/05/15,1086773.56,725.6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.26668,0,1.122,0,3.0396,0,6.7465,0,8.294,0,11.2435,0,12.3315,0,16.794,0,14.8515,0,21.4255,0,21.692,0,22.5385,0,27.124,0,0,0,26.053,0,0,0,18.0535,0,18.218,0,18.17,0,11.8545,0,8.0365,0,8.118,0,8.272,0,3.5705,0,2.1408,0,0.6293,0,0.06808,0.02872,0,0.07612,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"""

      import com.lightsource.monitoring.configs.meterreadings.{aslgeneration => aslg}
      aslg.readingTimes(srcLine)(0).toString() must_== "2015-05-18T00:30:00.000Z"
      aslg.readingTimes(srcLine).last.toString() must_== "2015-05-19T00:00:00.000Z"

      new MeterDataHandling[String, String]().process(srcLine, new TimeSeriesStringToStringFunctions(FongoStorage))

      println(FongoStorage.meterdb.getCollection("readings").find().next())

      new DateTime(
        FongoStorage.meterdb.getCollection("readings").find().next().get("timestamp").asInstanceOf[Date]).
        toDateTime(DateTimeZone.UTC).
        toString() must_== "2015-05-18T00:30:00.000Z"

      ok

    }
  }

}