package com.lightsource.monitoring

import java.nio.file.Path
import org.joda.time.LocalDate
import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage, MongoStorage}
import com.lightsource.monitoring.alerting.AlarmActioner
import grizzled.slf4j.Logging
import com.lightsource.monitoring.configs.meterreadings.MeterTypes
import org.joda.time.DateTime
import java.nio.file.Files
import java.nio.charset.StandardCharsets

class MeterDataCsvFunctions2(val rep: MonitoringStorage) extends TimeseriesFunctions2[String] with Logging {

  val makeParseableRawMeterData: String => Option[String] = singleMeterData => MeterLineTypeFinding.makeParseable(singleMeterData)

  val parseSingleMeterData: String => TimeSeries = smd => {
    require(MeterTypes.getTypeOfReading(smd).isDefined)
    logger.info("parsing line: " + smd)
    val dayext = MeterTypes.getTypeOfReading(smd).get
    TimeSeries(dayext.meaning(smd), dayext.meterId(smd), dayext.date(smd), dayext.readingTimes(smd), 
        dayext.values(smd).map(_.doubleValue), dayext.whatsVisible(smd), dayext.unit(smd))
  }

  override val createOtherTimeseriesFrom = (ts: TimeSeries) => List.empty[TimeSeries]
  
  val storeShowing: TimeSeries => Option[Int] = ts => rep.storeShowing(ts)

}