package com.lightsource.monitoring.configs.meterreadings

import java.io.File
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.joda.time.LocalDate
import com.lightsource.monitoring.MeterFileOpening
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.mongodb.BasicDBObject
import java.nio.file.Files
import org.vertx.java.core.json.JsonObject
import scala.collection.mutable.ListBuffer

@RunWith(classOf[JUnitRunner])
class FileTypesTest extends Specification {

  "FileTypes" should {

    "respond with defaults unless supportedfiletypes value is passed as config" in {
      
      import scala.collection.JavaConversions._
      
      println ( new JsonObject(Map("a" -> "b")) )
      
      val l: java.util.List[String] = bufferAsJavaList(ListBuffer(List("a", "b"): _*) )
      val l2: java.util.List[String] = ListBuffer(List("a", "b"): _*) 
      val l3: java.util.List[String] = ListBuffer("a", "b") 
      val l4 = bufferAsJavaList(ListBuffer("a", "b") )
      val l5 = ListBuffer("a", "b")

      println ( new JsonObject(Map("supportedfiletypes" -> l3)).toString())
      
      FileTypes( new JsonObject(Map("a" -> "b")).toString()).supported must_==List("csv", "xml")
      FileTypes( new JsonObject(Map("supportedfiletypes" -> "b")).toString()).supported must_==List("b")
      FileTypes( new JsonObject(Map("supportedfiletypes" -> bufferAsJavaList(ListBuffer("a", "b") ))).toString()).supported must_==List("a", "b")

      ok
    }
  }
}