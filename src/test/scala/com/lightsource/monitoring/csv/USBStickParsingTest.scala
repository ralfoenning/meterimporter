package com.lightsource.monitoring.csv

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.configs.meterreadings.MeterTypes
import com.lightsource.monitoring.configs.meterreadings.usb.readingTimes
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class USBStickParsingTest extends Specification {

  sequential


  "parsing usb meter reading on a low level" should {
    
    "work" in {

      val srcLine = "Private_38952453,11/12/2014,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.047,0.462,1.062,0.759,0.701,1.079,1.665,1.838,1.103,2.647,1.464,1.01,0.619,0.493,0.21,0.039,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"

      println(srcLine.split(",").length)
      println(MeterTypes.getTypeOfReading(srcLine))

      import com.lightsource.monitoring.configs.meterreadings.usb._
      
      readingTimes(srcLine)(0).toString() must_== "2014-12-11T00:30:00.000Z"
      readingTimes(srcLine).last.toString() must_== "2014-12-12T00:00:00.000Z"

      com.lightsource.monitoring.configs.meterreadings.usb.values(srcLine).size must_== 48

      ok
    }
  }
}