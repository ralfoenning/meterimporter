package com.lightsource.monitoring.meterimport

import java.io.File
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.joda.time.LocalDate
import com.lightsource.monitoring.MeterFileOpening
import com.lightsource.monitoring.mongo.FongoStorage
import com.lightsource.monitoring.MeterDataCsvFunctions
import com.mongodb.BasicDBObject
import java.nio.file.Files
import com.lightsource.monitoring.MeterDataXml
import com.lightsource.monitoring.MeterDataHandling
import java.nio.file.Path
import scala.xml.Node

@RunWith(classOf[JUnitRunner])
class ConsumptionCreationTest1 extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "simpleCase subtracting one full day from another" should {

    "work" in {

      val aslf = fileResource("asl-20150519.csv")
      new MeterDataHandling[String, Path]().process(aslf.toPath(), new MeterDataCsvFunctions(FongoStorage))
      val imsf = fileResource("imserv1day1meter_241.xml")
      new MeterDataHandling[Node, Path].process(imsf.toPath(), new MeterDataXml(FongoStorage))

      val res = FongoStorage.meterdb.getCollection("readings").find().toArray()
      import scala.collection.JavaConversions._
      println(res.toList.groupBy(_.get("type")).size)

      println(res.toList.filter(_.get("type").asInstanceOf[String] == "generation").size)
      println(res.toList.filterNot(_.get("type").asInstanceOf[String] == "generation").size)

      val timeseriesdiff = res.toList.filter(_.get("type").asInstanceOf[String] == "generation").zip(res.toList.filter(_.get("type").asInstanceOf[String] == "export")).map {

        tup =>
          {
            val gendbo = tup._1
            val expdbo = tup._2

            val genVal = gendbo.get("value").asInstanceOf[Double]
            val expVal = expdbo.get("value").asInstanceOf[Double]

            genVal - expVal

          }
      }
      
      println(timeseriesdiff)


      ok
    }
  }
}