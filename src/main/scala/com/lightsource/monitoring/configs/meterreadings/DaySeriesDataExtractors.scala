package com.lightsource.monitoring.configs.meterreadings

import org.joda.time.{ LocalDate, DateTime }
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import scala.xml.Node

trait DaySeriesExtractor[T] {
  val whatsVisible: T => Option[String] = data => None    //None means: n/a, Some("") means: should be there but isn't
  val meaning: T => String
  val meterId: T => String
  val values: T => List[String]
  val date: T => LocalDate
  /** Produce a list of dates in UTC */
  val readingTimes: T => List[DateTime]
  val unit: T => Option[String] = data => None
}

object common {

  val calculateReadingTimes: (String => LocalDate) => String => List[DateTime] = calculateDate => line => {
    val csv = calculateDate(line)
    val midnightSansTZ = new LocalDateTime(csv.getYear, csv.getMonthOfYear, csv.getDayOfMonth, 0, 0, 0)
    val midnightWithTZ = midnightSansTZ.toDateTime(DateTimeZone.UTC)

    val times: List[DateTime] = {
      val numberNeeded = 48
      def halfHoursStartingAt(x: DateTime): Stream[DateTime] = x #:: halfHoursStartingAt(x.plusMinutes(30))
      halfHoursStartingAt(midnightWithTZ.plusMinutes(30)) take 48 toList
    }
    times
  }
}

object aslgeneration extends DaySeriesExtractor[String] {

	val meaning: (String) => String = line => "generation"
  val meterId: String => String = line => line.split(",")(1).trim()
  val values: String => List[String] = line => line.split(",").drop(5).grouped(2).map(_.head).toList
  val date: String => LocalDate = line =>
    DateTimeFormat.forPattern("HH:mm:ss E dd/MM/yy").parseDateTime(line.split(",")(2)).toLocalDate

  val readingTimes: String => List[DateTime] = line => common.calculateReadingTimes(date)(line)

  override val whatsVisible: String => Option[String] = line => Some(line.split(",")(3).trim())

  override val unit: String => Option[String] = l => Some("kwh")


}

object usb extends DaySeriesExtractor[String] {

	val meaning: (String) => String = line => "something"
  val meterId: String => String = line => line.split(",")(0).trim()
  val values: String => List[String] = line => line.split(",").drop(2).toList
  val date: String => LocalDate = line =>
    DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(line.split(",")(1)).toLocalDate

  val readingTimes = (line: String) => common.calculateReadingTimes(date)(line)
  override val unit: String => Option[String] = l => Some("kwh")
}

object imserv extends DaySeriesExtractor[Node] {

	val meaning: (Node) => String = node => "export"
  val meterId: Node => String = node => {
    (node \ "@Label").
          map(_.head.text).
          flatMap { s => "(\\d*).*".r.findFirstMatchIn(s).map(_ group 1) }.
          filterNot(_.isEmpty()).head
  }
  val values: Node => List[String] = node => {
    (node \\ "Chart1_CategoryGroup").
          map(_ \\ "Value").map(_ \ "@Y").map(_.text).toList
  }
  val date: Node => LocalDate = line => readingTimes(line).head.toLocalDate()

  lazy val readingTimes = (node: Node) => {
	  val res = (node \\ "Chart1_CategoryGroup").
          map(_ \ "@Label").map(_.text)
    
       val res2=   res.map { x => DateTimeFormat.forPattern("dd/MM/yyyy HH:mm").parseLocalDateTime(x).toDateTime(DateTimeZone.UTC) }
    
    res2.toList
  }
  override val unit: Node => Option[String] = l => Some("kwh")
}

