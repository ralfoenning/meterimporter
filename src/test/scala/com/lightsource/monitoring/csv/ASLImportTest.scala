package com.lightsource.monitoring.csv

import java.io.File
import java.nio.file.{Files, Path}
import java.util.Date
import com.lightsource.monitoring.{MeterDataCsvFunctions, MeterDataHandling}
import com.lightsource.monitoring.mongo.FongoStorage
import com.mongodb.BasicDBObject
import org.joda.time.{DateTime, DateTimeZone}
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.casbah.commons.MongoDBObject

@RunWith(classOf[JUnitRunner])
class ASLImportTest extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "importing asl csv file into mongodb" should {

    "work" in {

      FongoStorage.meterdb.getCollection("readings").drop()

      //      val f = fileResource("ASLAlarmDemo_cmdReport20150311040006.csv")
      val f = fileResource("asl-20150519.csv")
      new MeterDataHandling[String, Path]().process(f.toPath(), new MeterDataCsvFunctions(FongoStorage))
      
      FongoStorage.countReadings(MongoDBObject()) must_==97    //48 readings, 1 showing, 48 generation-pence (NaN)

      val dbo = new BasicDBObject()
      val reading = FongoStorage.findOneReading(dbo, dbo, dbo)
      reading.map(_.get("type")) must_== Some("generation")

      val res = FongoStorage.meterdb.getCollection("readings").find(new BasicDBObject("type", "generation") )
      val dayOfWeekOfFirstStoredTimestamp = new DateTime(
          res.next().get("timestamp").asInstanceOf[Date]).
          toDateTime(DateTimeZone.UTC).getDayOfWeek
          
      for (r <- 1 to res.size()-2) res.next()
      
      val dayOfWeekOfLastStoredTimestamp = new DateTime(
          res.next().get("timestamp").asInstanceOf[Date]).
          toDateTime(DateTimeZone.UTC).getDayOfWeek 
          
      dayOfWeekOfLastStoredTimestamp must_== dayOfWeekOfFirstStoredTimestamp+1
      
      import scala.collection.JavaConversions._
      val dateInLine = new MeterDataCsvFunctions(FongoStorage).parseSingleMeterData(Files.readAllLines(f.toPath()).head).date
      FongoStorage.hasOldOngoingAlarm(meterid = "14172974", startingOnOrBefore = dateInLine) must_== true

      FongoStorage.meterdb.getCollection("readings").find(new BasicDBObject("type", "generation-showing") ).size() must_== 1
      
      val dboOfTypeShowing = FongoStorage.meterdb.getCollection("readings").find(new BasicDBObject("type", "generation-showing") ).next()
      
      dboOfTypeShowing.get("value").toString() must_== "1086773.56"
      new DateTime(
        dboOfTypeShowing.get("timestamp").asInstanceOf[Date]).
        toDateTime(DateTimeZone.UTC).
        toString() must_== "2015-05-18T00:30:00.000Z"

      ok
    }
  }
}