package com.lightsource.monitoring.configs.meterreadings

object MeterTypes {

  private val idtype =
    Map(
      "14172978" -> aslgeneration,
      "14172974" -> aslgeneration,
      "1234" -> usb)

  private val typesize =
    Map(
      aslgeneration -> 101,
      usb -> 50)

  def getMeterLineExtractor(meterId: String) = idtype.get(meterId)
  
  def getExpectedSizeInLineFor(rtype: DaySeriesExtractor[String]) = typesize.get(rtype)

  val knownMeterTypes = List(aslgeneration, usb)

  def getTypeOfReading(line: String) : Option[DaySeriesExtractor[String]] = {

    line.split(",", -1) match {
      case ls if ls.size == 101 => Some(aslgeneration)
      case ls if ls.size == 50  => Some(usb)
      case _                    => None
    }
  }
}