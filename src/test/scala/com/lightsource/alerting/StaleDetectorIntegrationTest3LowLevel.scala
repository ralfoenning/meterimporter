package com.lightsource.integrationtests

import scala.collection.immutable.Stream.consWrapper
import scala.runtime.ScalaRunTime
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.json4s.JsonAST.JNothing
import org.json4s.JsonAST.JString
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL.double2jvalue
import org.json4s.JsonDSL.jobject2assoc
import org.json4s.JsonDSL.pair2Assoc
import org.json4s.JsonDSL.pair2jvalue
import org.json4s.JsonDSL.string2jvalue
import org.json4s.jvalue2monadic
import org.json4s.native.JsonMethods.pretty
import org.json4s.native.JsonMethods.render
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.json4s.native.JsonParser
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import java.net.URLEncoder
import com.lightsource.monitoring.common.MongoStorage
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import com.mongodb.casbah.commons.MongoDBObject
import com.github.fakemongo.Fongo
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaLocalDateTimeConversionHelpers

@RunWith(classOf[JUnitRunner])
class StaleDetectorIntegrationTest3LowLevel extends Specification {

  "create alarm" should {

    "work low level" in {

      import com.mongodb.casbah.Imports.wrapDBObj
      RegisterJodaTimeConversionHelpers()

      val alarmscoll = new Fongo("fakemongo").getDB("meterdb").getCollection("alarms")

      alarmscoll.insert(MongoDBObject("_id" -> "idaaa") ++
        ("installation" -> null) ++
        ("description" -> "No Production") ++
        ("meterId" -> "idaaa") ++
        ("timestamp" -> DateTime.now().minusDays(1)) ++
        ("to" -> null))

      alarmscoll.count() must_== 1
      val dtForCasbah = LocalDate.now().toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
      import com.mongodb.casbah.Imports._

      alarmscoll.count(("timestamp" $lte dtForCasbah) ++ ("meterId" -> "idaaa") ++ ("to" -> null)) must_== 1
      alarmscoll.count(("timestamp" $lte DateTime.now()) ++ ("meterId" -> "idaaa") ++ ("to" -> null)) must_== 1
      
      ok
    }
  }
}