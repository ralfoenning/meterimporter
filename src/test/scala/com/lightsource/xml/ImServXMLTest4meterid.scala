package com.lightsource.xml

import java.io.File
import scala.xml.Node
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.specs2.mutable.Specification
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ImServXMLTest4meterid extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())
  def streamResource(resourcePath: String) = this.getClass().getResourceAsStream("/" + resourcePath)

  "reading imserv xml file" should {

    val onedayPlusGuff =
      <Report Name="RPT_CON2_BasicReportBuilder" Textbox22="***** No Data Available for the selected report parameters *****" Textbox76="Report displays data in Time Period End" xmlns="RPT_CON2_BasicReportBuilder" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="RPT_CON2_BasicReportBuilder http://edvsql02lsc:8080/ReportServer?%2FEDV_Reports%2FBasicReportBuilder%2FRPT_CON2_BasicReportBuilder&amp;rs%3AFormat=XML&amp;rs%3ASnapshot%3Aisnull=True&amp;rc%3ASchema=True">
        <Tablix5>
          <Textbox33 Company_Name="Lightsource Midscale Limited" Textbox33="Company Name"/>
          <Textbox35 Measure_Type="Electricity" Textbox35="Measure Type"/>
          <Textbox263 MeasureLevelType_Name="Multiple" Textbox263="Measure Level Type"/>
          <Textbox37 MeasurementQuantity_Name="Consumption" Textbox37="Measure Quantity Converted To"/>
          <Textbox39 Measurementunit_Name="kWh" Textbox39="Measurement Unit Converted To"/>
        </Tablix5>
        <Tablix8>
          <Textbox41 Textbox41="Report At" Textbox42="Measure"/>
          <Textbox2 Textbox2="Aggregate Level" Textbox20="30 (minutes)"/>
          <Textbox249 Textbox249="Start Date" Textbox250="22/04/2015"/>
          <Textbox251 Textbox251="End Date" Textbox252="23/04/2015"/>
          <Textbox235 Textbox235="Transaction Timestamp" Textbox236="29/04/2015 19:04:00"/>
        </Tablix8>
        <Tablix6>
          <Textbox43 Textbox43="Portfolio Item 1" Textbox48="214669932 kWh Import"/>
          <Textbox44 Textbox44="Portfolio Item 2" Textbox49="Holme Mills POC 1 Import"/>
          <Textbox45 Textbox45="Portfolio Item 2"/>
          <Textbox46 Textbox46="Portfolio Item 4"/>
          <Textbox47 Textbox47="Portfolio Item 5"/>
        </Tablix6>
        <Chart1>
          <Chart1_SeriesGroup_Collection>
            <Chart1_SeriesGroup Label="kWh Import">
              <Chart1_CategoryGroup_Collection>
                <Chart1_CategoryGroup Label="22/04/2015 00:30">
                  <Value Y="1.8"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:00">
                  <Value Y="1.5"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 23:30">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
                <Chart1_CategoryGroup Label="22/04/2015 00:00">
                  <Value Y="1.6"/>
                </Chart1_CategoryGroup>
              </Chart1_CategoryGroup_Collection>
            </Chart1_SeriesGroup>
          </Chart1_SeriesGroup_Collection>
        </Chart1>
      </Report>
    "work" in {

      println("===Meter Id===")
      val reg = "(\\d*).*".r                                  //needs assignment to be able to use reg(mid) 
      val res = "214669932 " match { case reg(mid) => mid}    //reg(mid) is a pattern, not a parameter
      println(res)
      println("(\\d*).*".r.findFirstMatchIn("214669932 ").isDefined)
      println("(\\d*).*".r.findFirstMatchIn("214669932 ").map { regexmatch => regexmatch.group(1) })
      println("(\\d*).*".r.findFirstMatchIn("214669932 ").map(_ group 1) )
      
      
      println("a"+ (onedayPlusGuff \\ "Textbox43").collect { case textboxnode: Node => textboxnode.attributes.get("Textbox48").map { x => x.head.text match { case reg(mid) => mid }} } )
      println("b"+ (onedayPlusGuff \\ "Textbox43").map(_ \ "@Textbox48").map { x => x.head.text match { case reg(mid) => mid }} )
      println("c"+ 
          (onedayPlusGuff \\ "Textbox43").
          map(_ \ "@Textbox48").
          map(_.head.text).
          map { s => "(\\d*).*".r.findFirstMatchIn(s).map(_ group 1) } )
          
          (onedayPlusGuff \\ "Textbox43").
          map(_ \ "@Textbox48").
          map(_.head.text).
          map { s => "(\\d*).*".r.findFirstMatchIn(s).map(_ group 1) }.
          head must_== Some("214669932")

      ok
    }
  }
}