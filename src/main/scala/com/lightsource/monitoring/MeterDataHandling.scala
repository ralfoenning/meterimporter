package com.lightsource.monitoring

import com.lightsource.monitoring.common.TimeSeries
import grizzled.slf4j.Logging

class MeterDataHandling[CONTENTTYPE, HOLDER] extends Logging {

  def process(data: HOLDER, md: TimeseriesFunctions[CONTENTTYPE, HOLDER]): Seq[TimeSeries] = {
    
    val listOfTimeseries = md.extractListOfTimeSeries(data)    //one for each meterid
    
    val listOfParseableSingleMeterDatas = listOfTimeseries flatMap { x => md.makeParseableRawMeterData(x) }
    
    val listOfSingleMeterTimeSeries : Seq[TimeSeries] =     //singleMeterTimeSeries = meaning-meterid-date-timesList-valuesList
      listOfParseableSingleMeterDatas.map { x => md.parseSingleMeterData(x) }                        
    
    val listOfSavedTimeseries = listOfSingleMeterTimeSeries.flatMap { timeseries => md.storeSingleMeterData(timeseries) }
    
//    val listOfTimeseriesWithAShowing = listOfSavedTimeseries.flatMap { timeseries => timeseries.showing.map { ts => timeseries } }
//    listOfTimeseriesWithAShowing.map { timeseries => md.s }
    
    val alertingActions = listOfSavedTimeseries.flatMap { storedtimeseries => md.produceAlarmActions(storedtimeseries) }
    alertingActions.map { x => x.doit() }

//    val listOfDerivedTimeseries = listOfSingleMeterTimeSeries.flatMap{ t => md.createOtherTimeseriesFrom(t) }
//    listOfDerivedTimeseries.map { newtimeseries => md.storeSingleMeterData(newtimeseries) }

    listOfSingleMeterTimeSeries
  }

}



