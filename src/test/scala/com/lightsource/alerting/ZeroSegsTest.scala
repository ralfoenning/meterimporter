package com.lightsource.alerting

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.integrationtests.alerting
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ZeroSegsTest extends Specification {

  val threeSegmentsOfZeros: List[Double] = List(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1.799, 16.507, 29.3595, 44.3595, 77.3595, 129.765, 202.035, 266.3595, 277.3595, 333.3595, 339.595, 0.0, 141.61, 95.735, 89.1, 62.665, 40.678, 22.712, 5.2345, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

  "zeroSegs" should {

    "work" in {

      alerting.zeroSegs((1 to threeSegmentsOfZeros.size).zip(threeSegmentsOfZeros)) must_==
        List(
          List((14, 0.0), (13, 0.0), (12, 0.0), (11, 0.0), (10, 0.0), (9, 0.0), (8, 0.0), (7, 0.0), (6, 0.0), (5, 0.0), (4, 0.0), (3, 0.0), (2, 0.0), (1, 0.0)),
          List((26, 0.0)),
          List((48, 0.0), (47, 0.0), (46, 0.0), (45, 0.0), (44, 0.0), (43, 0.0), (42, 0.0), (41, 0.0), (40, 0.0), (39, 0.0), (38, 0.0), (37, 0.0), (36, 0.0), (35, 0.0), (34, 0.0)))
    }
  }

  "hasStartGap" should {
    "work" in {

      alerting.hasStartGap(threeSegmentsOfZeros.slice(21, 48 - 21)) must beFalse
    }
  }

  "alarm start" should {

    "work" in {

      val times = List(1.0, 2.000, 3.000, 4.0, 5.0, 6.000)
      val values = List(0.0, 1.361, 1.377, 0.0, 0.0, 1.365)

      alerting.zeroSegs(times.zip(values)).map(_.reverse) must_==
        List(
          List((1.0, 0.0)),
          List((4.0, 0.0), (5.0, 0.0)))

      val alarmStart = alerting.zeroSegs(times.zip(values)).map(_.reverse) match {
        case h :: t => {
          val earliestTime = h.head._1
          Some(earliestTime)
        }
        case Nil => None
      }

      alarmStart must_== Some(1.0)

    }
  }

}