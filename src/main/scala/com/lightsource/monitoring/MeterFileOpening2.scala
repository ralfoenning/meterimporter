package com.lightsource.monitoring

import java.nio.file.Files
import java.nio.file.Path
import grizzled.slf4j.Logging
import java.nio.charset.StandardCharsets
import java.io.IOException
import scala.reflect.runtime.{ universe => ru }

object MeterFileOpening2 extends Logging {

  private[monitoring] def access(p: Path): Path = {

    import scala.collection.JavaConversions._

    var count = 5
    while (!FileLockingUtils.canBeExclusivelyLocked(p.toFile()) && count > 0) {
      count = count - 1
      Thread.sleep(200)
    }

    if (!FileLockingUtils.isWriteableAndThereforeCompletelyWritten(p.toFile()))
      throw new IOException("!isWriteableAndThereforeCompletelyWritten: " + p.toFile())

    p

  }
}