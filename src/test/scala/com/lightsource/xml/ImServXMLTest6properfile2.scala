package com.lightsource.xml

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ImServXMLTest6properfile2 extends Specification {

  """obtaining meterid from 
    <Chart1><Chart1_SeriesGroup_Collection><Chart1_SeriesGroup Label="kWh Import">...<Chart1_SeriesGroup Label="214669932 kWh Import">""" should {

    val uri = this.getClass().getResource("/" + "imserv1day2meters_241-232.xml").toURI()
    val p = Paths.get(uri)

    "work" in {

      val xmlStraight = new String(Files.readAllBytes(p), StandardCharsets.UTF_8)

      val xmlString = xmlStraight.trim().replaceFirst("^([\\W]+)<", "<") //content-is-not-allowed-in-prolog

      val res = scala.xml.XML.loadString(xmlString)
      
      val res2 = (res \\ "Chart1_SeriesGroup")
      
      println(res2.toList)

      val listOfPossibleMeterIds =
        (res \\ "Chart1_SeriesGroup").
          flatMap(_ \ "@Label").
          map(_.head.text).
          flatMap { s => "(\\d*).*".r.findFirstMatchIn(s).map(_ group 1) }.
          filterNot(_.isEmpty())

      println (listOfPossibleMeterIds)
          
      listOfPossibleMeterIds must_==List("2394000121241", "2394000121232")

      ok
    }
  }
}