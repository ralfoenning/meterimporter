package com.lightsource.monitoring

import com.lightsource.monitoring.common.TimeSeries
import grizzled.slf4j.Logging

class MeterDataHandling2[CONTENTTYPE, HOLDER] extends Logging {

  def process(data: HOLDER, extf: HOLDER => List[CONTENTTYPE], md: TimeseriesFunctions2[CONTENTTYPE]): Seq[TimeSeries] = {
    
    val listOfTimeseries = extf(data)    //one for each meterid
    
    val listOfParseableSingleMeterDatas = listOfTimeseries flatMap { x => md.makeParseableRawMeterData(x) }
    
    val listOfSingleMeterTimeSeries : Seq[TimeSeries] =     //singleMeterTimeSeries = meaning-meterid-date-timesList-valuesList
      listOfParseableSingleMeterDatas.map { x => md.parseSingleMeterData(x) }                        
    
    val actuallyStoredOriginalTimeseriesList = listOfSingleMeterTimeSeries.flatMap { timeseries => md.storeSingleMeterData(timeseries) }
    
//    val listOfTimeseriesWithAShowing = listOfSavedTimeseries.flatMap { timeseries => timeseries.showing.map { ts => timeseries } }
//    listOfTimeseriesWithAShowing.map { timeseries => md.s }
    
    val alertingActions = actuallyStoredOriginalTimeseriesList.flatMap { storedtimeseries => md.produceAlarmActions(storedtimeseries) }
    alertingActions.map { x => x.doit() }

//    val listOfDerivedTimeseries = listOfSingleMeterTimeSeries.flatMap{ t => md.createOtherTimeseriesFrom(t) }
//    listOfDerivedTimeseries.map { newtimeseries => md.storeSingleMeterData(newtimeseries) }

    listOfSingleMeterTimeSeries
  }

}



