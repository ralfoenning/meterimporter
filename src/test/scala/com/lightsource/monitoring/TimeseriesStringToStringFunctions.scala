package com.lightsource.monitoring

import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage}
import com.lightsource.monitoring.configs.meterreadings.MeterTypes
import grizzled.slf4j.Logging

class TimeSeriesStringToStringFunctions(val rep: MonitoringStorage) extends TimeseriesFunctions[String,String] with Logging {

  def extractListOfTimeSeries(s: String): Seq[String] = s.split("\n").toList

  val makeParseableRawMeterData: String => Option[String] = singleMeterData => MeterLineTypeFinding.makeParseable(singleMeterData)

  val parseSingleMeterData: String => TimeSeries = smd => {
    require(MeterTypes.getTypeOfReading(smd).isDefined)
    logger.info("parsing line: " + smd)
    val dayext = MeterTypes.getTypeOfReading(smd).get
    logger.info(dayext)
    TimeSeries(dayext.meaning(smd), dayext.meterId(smd), dayext.date(smd), dayext.readingTimes(smd), dayext.values(smd).map(_.doubleValue), dayext.whatsVisible(smd))
  }

  val storeShowing: TimeSeries => Option[Int] = ts => rep.storeShowing(ts)

}