package com.lightsource.monitoring

import com.lightsource.monitoring.configs.meterreadings.MeterTypes

import grizzled.slf4j.Logging

object MeterLineTypeFinding extends Logging {

  val makeParseable: String => Option[String] = line => {

    val lineIsParseable = MeterTypes.getTypeOfReading(line).isDefined
    if (!lineIsParseable) logger.warn("LINE TYPE NOT DETECTABLE BY SIZE: " + line.substring(0, Math.min(40, line.size)) + "...")

    MeterTypes.getTypeOfReading(line) match {
      case Some(_) => Some(line)
      case None => {
        val potentialMeterIds = MeterTypes.knownMeterTypes.map { x => x.meterId(line) }
        val extractorsBasedOnMeterIdsFound = potentialMeterIds.flatMap(mid => MeterTypes.getMeterLineExtractor(mid) )
        extractorsBasedOnMeterIdsFound match {
          case Nil => None
          case extractor :: _ => {
            val correctSize = MeterTypes.getExpectedSizeInLineFor(extractor).get
            val numMissing = correctSize - line.split(",", -1).size
            Some(line + "," + (List.fill[String](numMissing)("0.0") mkString "," ) )
          }
        }
      }
    }
  }
}