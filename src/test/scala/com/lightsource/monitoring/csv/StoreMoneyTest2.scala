package com.lightsource.monitoring.csv

import java.util.Date
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.specs2.mutable.Specification
import com.lightsource.monitoring.MeterDataHandling
import com.lightsource.monitoring.TimeSeriesStringToStringFunctions
import com.lightsource.monitoring.configs.meterreadings.{ aslgeneration => aslg }
import com.lightsource.monitoring.mongo.FongoStorage
import org.specs2.runner.JUnitRunner
import org.junit.runner.RunWith
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import com.lightsource.monitoring.MeterDataCsvFunctions
import java.nio.file.Path
import com.lightsource.monitoring.TimeSeriesExtractorFunctions
import com.lightsource.monitoring.MeterDataHandling2
import com.lightsource.monitoring.MeterDataCsvFunctions2
import com.mongodb.DB

@RunWith(classOf[JUnitRunner])
class StoreMoneyTest2 extends Specification {

  sequential

  "generation-pence from generation-kwh" should {

    "work" in {
      
      FongoStorage.meterdb.asInstanceOf[DB].dropDatabase()

      FongoStorage.meterdb.getCollection("tariffs").insert(
        MongoDBObject("meterId" -> "EML1411033033") ++
          ("timestamp" -> new DateTime(2015, 3, 25, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
          ("rate" -> "0.123"))

      FongoStorage.meterdb.getCollection("readings").drop()

      val srcLine = """Demo21,EML1411033033   ,04:08:16 Wed 25/03/15,2640319,5198,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0,0,0.001,0,        0.001,0,0.001,0.005,0.001,0.124,0,0.338,0,0.633,0,0.877,0,1.101,0,1.295,0,1.344,0,1.457,0,1.247,0,1.589,0,1.501,0,1.752,0,1.728,0,1.431,0,1.593,0,1.425,0,        0.885,0,0.85,0,0.392,0,0.136,0,0.089,0,0.018,0,        0,0.002,0,0,0,0.001,0,0,0,0.001,0,0,0,0.001,0,0.001,0,0,0,0.001,0,0"""

      new MeterDataHandling2[String, String]().process(srcLine, TimeSeriesExtractorFunctions.csvstringextract, new MeterDataCsvFunctions2(FongoStorage))

      FongoStorage.meterdb.getCollection("readings").count() must_== 48 + 1 + 48
      FongoStorage.meterdb.getCollection("totals").count() must_== 2

      val sumthenmult = aslg.values(srcLine).map(_.toDouble).foldLeft(0.0)(_ + _) *
        FongoStorage.getRateForTimeAndMeterId(new DateTime(2015, 3, 25, 1, 2, 3).toDateTime(DateTimeZone.UTC), "EML1411033033").get

      println(sumthenmult)
      println(Math.round(2.68263 * 10))
      println(Math.round(2.7044400000000004 * 10))

      import scala.collection.JavaConversions._
      val multthensum = FongoStorage.meterdb.getCollection("totals").find().toArray().toList.reverse.head.get("value").toString().toDouble
      println(multthensum)
      
      Math.round(sumthenmult * 10) must_== Math.round(multthensum * 10)

      ok

    }
  }

}