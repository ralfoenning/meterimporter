package com.lightsource.monitoring.derived

import com.lightsource.monitoring.common.{ TimeSeries, MonitoringStorage }
import grizzled.slf4j.Logging
import org.joda.time.DateTime

class DerivedConsumptionkwhFromGeneration(rep: MonitoringStorage) extends DerivedTimeseries with Logging {

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries] = generationts => {

    val exportmid = rep.findPostProcessingTimeseries(generationts.mid)
    lazy val expEntry = (t: DateTime) => rep.entriesForInInterval(exportmid, t, t).headOption

    val commonTimesbyGenerationVal = (generationts.times.zip(generationts.values)).filterNot { t => expEntry(t._1).isEmpty }

    val commonTimesbyconsumptionVal = commonTimesbyGenerationVal.map { t =>
      {
        val commonTime = t._1
        val genval = t._2

        val consumptionval = expEntry(commonTime).map { genEntry => genEntry.get("value").asInstanceOf[Double] - genval }.map { num => Math.abs(num) }.get
        (commonTime, consumptionval)
      }
    }
    
    commonTimesbyconsumptionVal match {
      case Nil => Nil
      case _ => TimeSeries("consumption", generationts.mid + "-" + exportmid,
      generationts.date, commonTimesbyGenerationVal.map(_._1), commonTimesbyconsumptionVal.map(_._2), None, Some("kwh")) :: Nil
    }

  }

  //=============derived version 2===============

  trait Op[T] { def eval: T }
  trait NameOp extends Op[String]
  trait IdOp extends Op[String]
  trait ValueOp extends Op[Number]
  case class ConsumptionNameOp(args: String*) extends NameOp { def eval: String = "consumption" }
  case class ConsumptionIdOp(args: String*) extends IdOp { def eval: String = args.mkString("-") }
  case class GenerationMinusExportOp(genVal: Double, expVal: Double) extends ValueOp { def eval: Number = genVal - expVal }

  val createDerived: List[TimeSeries] => (NameOp, IdOp, ValueOp) => TimeSeries = tslist => (nop, iop, vop) => {

    tslist match {
      case List(ts1, ts2) => {

        def inboth[T](l1: List[T], l2: List[T]): List[T] = {
          val desc = List(l1, l2).sortBy(_.size)
          desc(0).diff(desc(0).diff(desc(1)))
        }

      }
    }

    null
  }
}