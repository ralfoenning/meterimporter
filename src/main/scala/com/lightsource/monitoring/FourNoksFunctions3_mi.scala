package com.lightsource.monitoring

import java.nio.file.Path
import org.joda.time.LocalDate
import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage, MongoStorage}
import com.lightsource.monitoring.alerting.AlarmActioner
import grizzled.slf4j.Logging
import com.lightsource.monitoring.configs.meterreadings.MeterTypes
import org.joda.time.DateTime
import java.nio.file.Files
import java.nio.charset.StandardCharsets
import org.joda.time.DateTimeZone
import scala.collection.JavaConversions._

object FourNoksHandler {
  
  def process(p: Path, rep: MonitoringStorage): Seq[TimeSeries] = {
    
    val res = Files.readAllLines(p, StandardCharsets.UTF_8).toList
    
    val res2 = res map parse
    
    val res3 = res2 flatten
    
    res3 map rep.storeTimeseries
    
    Nil
    
  }
    
  def parse(dataString: String): Seq[TimeSeries] = {

    val meaning: String => String = name => name match {
      //      case "generener" => "generation"
      //      case "soldener"  => "export"
      case _ => name
    }

    val isCumulative: String => Boolean = name => name match {
      case "generener" | "soldener" => true
      case _                        => false
    }

    import org.json4s.native.JsonMethods._
    import org.json4s._

    val json = org.json4s.native.JsonParser.parse(dataString)

    val mid = (json \\ "devsn").values.toString

    val datapertime = (json \\ "logdata").children.map(_.children.map { x => x.values.toString() }) //List(1436090400, 0.48, 1383.41, 0.04, 546.32, 0, 274.4, 34, 118.53, 0.44, 1111.49, 0, 0, 0), List(1436091300, 0.48, 1383.53, 
    val meanings = (json \\ "logvarlist").children.map(_ \\ "name").map(_.values.toString) //List(logutc, generpwr, generener, soldpwr, soldener, bougpwr, bougenertot, bougenerf1, bougenerf2, conspwr, consener, extalarm1, extalarm2, eliosflag)
    val units = (json \\ "logvarlist").children.map(_ \\ "unit").map(_.values.toString) //List(utc, kW, kWh, kW, kWh, kW, kWh, kWh, kWh, kW, kWh, *, *, *)

    val zip = datapertime.map { data => (data, meanings, units).zipped.toList } //List(List((1436090400,logutc,utc), (0.48,generpwr,kW), (1383.41,generener,kWh), (0.04,soldpwr,kW), 

    val makeTimeseries = (l: List[(String, String, String)]) => {

      val utcInSecs = l.head._1

      l.tail.map(t => {
        com.lightsource.monitoring.common.TimeSeries(t._2,
          mid + "-" + t._2,
          new DateTime(utcInSecs.toLong * 1000).toLocalDate(),
          List(new DateTime(utcInSecs.toLong * 1000).toDateTime(DateTimeZone.UTC)),
          List(t._1.toDouble),
          None,
          t._3 match { case "*" => None; case r @ _ => Some(r) },
          List(t._1.toDouble),
          false)

      })
    }

    val res3 = zip.map(makeTimeseries(_))
    res3.flatten

  }

}