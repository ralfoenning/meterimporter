package com.lightsource.monitoring

import java.nio.file.Path
import org.joda.time.{ DateTime, LocalDate }
import com.lightsource.monitoring.alerting.Alerting
import com.lightsource.monitoring.common.{TimeSeries, MonitoringStorage, MongoStorage}
import com.lightsource.monitoring.alerting.AlarmActioner
import grizzled.slf4j.Logging
import com.lightsource.monitoring.configs.meterreadings.MeterTypes
import java.nio.file.Files
import java.nio.charset.StandardCharsets
import scala.xml.Node
import com.lightsource.monitoring.configs.meterreadings.DaySeriesExtractor
import com.lightsource.monitoring.configs.meterreadings.imserv
import com.mongodb.BasicDBObject

class MeterDataXml(val rep: MonitoringStorage) extends TimeseriesFunctions[Node, Path] with Logging {

  def extractListOfTimeSeries(p: Path): Seq[Node] = {

    val xmlStraight = new String(Files.readAllBytes(p), StandardCharsets.UTF_8)

    val xmlString = xmlStraight.trim().replaceFirst("^([\\W]+)<", "<") //content-is-not-allowed-in-prolog

    val res = scala.xml.XML.loadString(xmlString)

    val res2 = (res \\ "Chart1_SeriesGroup")

    res2.toList

  }

  override val makeParseableRawMeterData: Node => Option[Node] = singleMeterData => Some(singleMeterData)

  override val parseSingleMeterData: Node => TimeSeries = smd => {

    val dayext = imserv

    TimeSeries(dayext.meaning(smd), dayext.meterId(smd), dayext.date(smd), dayext.readingTimes(smd), dayext.values(smd).map(_.doubleValue),
        dayext.whatsVisible(smd), dayext.unit(smd))
  }

    
    
    
  
  //=============derived version 2===============

  trait Op[T] { def eval: T }
  trait NameOp extends Op[String]
  trait IdOp extends Op[String]
  trait ValueOp extends Op[Number]
  case class ConsumptionNameOp(args: String*) extends NameOp { def eval: String = "consumption" }
  case class ConsumptionIdOp(args: String*) extends IdOp { def eval: String = args.mkString("-") }
  case class GenerationMinusExportOp(genVal: Double, expVal: Double) extends ValueOp { def eval: Number = genVal - expVal }

  val createDerived: List[TimeSeries] => (NameOp, IdOp, ValueOp) => TimeSeries = tslist => (nop, iop, vop) => {

    tslist match {
      case List(ts1, ts2) => {

        def inboth[T](l1: List[T], l2: List[T]): List[T] = {
          val desc = List(l1, l2).sortBy(_.size)
          desc(0).diff(desc(0).diff(desc(1)))
        }
        
        

      }
    }

    null
  }

}